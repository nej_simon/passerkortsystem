<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This library is used to manage PHP sessions, since CI sessions are buggy atm.
 */
Class Pks_session {
	
	public function __construct() {
		self::start_session();
	}

	private static function start_session() {
		
		/* This feels a bit stupid but make the cookie "never" expire
		   by setttings the lifetime to 10 years in the future. */
		$cookie_expire = time() + (10 * 365 * 24 * 60 * 60);
		session_set_cookie_params($cookie_expire);

		session_start();
	}

	/**
	 * Check if a user is logged in
	 * @param  string $as Either 'admin' or 'instructor'
	 * @return boolean true if a user of the type given as argument is logged in
	 */
	public function is_logged_in($as) {
	    
	    $login_info = $this->retrieve('login_info');
	    
	    if (!$login_info) {
	        log_message( 'debug', __FUNCTION__ . ": Login check failed! Current session data:  " . json_encode( $this->retrieve_all() ) );
	        return false;
	    }

	    log_message( 'debug', __FUNCTION__ . ": Found userdata: " . json_encode( $login_info ) );
	    
	    switch ($as) {
	        case 'admin':
	            return $login_info['admin'] == "1";
	        case 'instructor':
	            return $login_info['instructor'] == "1";
	        default:
	            return false;
	    }
	}

	/**
	 * Store data in the session object, possibly overwriting the previous value.
	 * @param  string $key
	 * @param  string $value
	 * @return null
	 */
	public function store( $key, $value ) {
		$_SESSION[$key] = serialize( $value );
	}

	/**
	 * Retrieve $key from session
	 * @param  string $key
	 * @return mixed The data stored with $key, otherwise null
	 */
	public function retrieve( $key ) {
		if (isset( $_SESSION[$key] )) {
			return unserialize( $_SESSION[$key] );
		} else {
			return NULL;
		}
	}

	/**
	 * Retrieve all data from session
	 * @return Array session data
	 */
	public function retrieve_all( ) {
		return $_SESSION;
	}

	/**
	 * Remove $key from session
	 * @param  string $key
	 * @return null
	 */
	public function remove( $key ) {
		unset( $_SESSION[$key] );
	}

	/**
	 * Log out
	 * @return null
	 */
	public function logout() {
		// Clear the session superglobal
		$_SESSION = array();

		// Remove the session cookie
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// Destroy the session and start a new
    	session_destroy();
    	self::start_session();
    	
	}

}