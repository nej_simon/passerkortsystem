<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define( 'FPDF_FONTPATH', dirname(__FILE__) . '/FPDF/font' );

require_once( dirname(__FILE__) . '/FPDF/fpdf.php' );
require_once( dirname(__FILE__) . '/FPDF/fpdi.php' );