<?php
/**
 * Misc util functions
 */

function print_time_options() {
    $output = '';

    for ( $i = 0; $i < 48; $i++ ) {
        if ( ( $j = $i/2 ) < 10 )
            $time = '0' . floor( $j );
        else
            $time = floor( $j );

        if ( $j === 12 )
            $selected = "selected";
        else 
            $selected = "";

        if ( ( $i % 2 ) === 0 )
            $time .= ':00';
        else
            $time .= ':30';

        $output .= "<option value='{$time}:00' {$selected}>{$time}</option>";
    }

    return $output;
}

$upload_errors = Array(
    0 => 'UPLOAD_ERR_OK',
    1 => 'UPLOAD_ERR_INI_SIZE',
    2 => 'UPLOAD_ERR_FORM_SIZE',
    3 => 'UPLOAD_ERR_PARTIAL',
    4 => 'UPLOAD_ERR_NO_FILE',
    5 => 'UPLOAD_ERR_NO_TMP_DIR',
    6 => 'UPLOAD_ERR_CANT_WRITE',
    7 => 'UPLOAD_ERR_EXTENSION'
);

function handle_photo_upload( $files ) {
    $ci =& get_instance();
    $ci->load->library( 'imageresize' );

    $tmpName   = $files["tmp_name"];
    $fileName  = $files["name"];
    $extension = '.' . strtolower( end( explode( ".", $fileName ) ) );
    
    try {
        $ci->imageresize->openImage( $tmpName, $extension );
    } catch ( Exception $e ) {
        die("Kunde inte öppna fotot, är det en korrekt bildfil?");
    }

    $photo_id = time();
    $filename = $photo_id . '.jpg';
    $thumb_filename = $photo_id . '_thumb.jpg';

    try {
        $ci->imageresize->resizeImage( 1024, 768, 'crop' );
        $ci->imageresize->saveImage( PHOTO_UPLOAD_DIR . "/", $filename, 75 );

        $ci->imageresize->resizeImage( 350, 450, 'crop' );
        $ci->imageresize->saveImage( PHOTO_UPLOAD_DIR . "/", $thumb_filename, 75 );

        return $filename;

    } catch ( Exception $e) {
        die("Kunde inte ändra storlek på fotot, är det en korrekt bildfil?");
    }
}

function get_photo_thumbnail( $photo ) {
    if ( ! $photo || ! file_exists( PHOTO_UPLOAD_DIR . "/" . $photo ) )
        return NULL;

    $photo_filename_parts = explode( '.', $photo );
    $photo_thumb = $photo_filename_parts[0] . '_thumb.' . $photo_filename_parts[1];

    return $photo_thumb;
}