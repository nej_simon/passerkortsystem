<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * TODO: Send an error code instead of a string to let the frontend handle translations instead? Or do translation some other way?
 * TODO: Comments
 */

class Regterminal extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('regterminal_model');

        log_message( 'debug', __FUNCTION__ . ": Current userdata: " . json_encode( $this->pks_session->retrieve_all() ) );
    }

    public function index() {
        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            redirect("/");
        }
        $this->load->view( 'pages/regterminal_view' );
        $this->load->view( 'templates/footer' );
    }

    public function init_registration($card_nr, $date, $time) {
        log_message('debug', __FUNCTION__ . ": Running");

        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            $data['errormsg'] = 'init_registration: Du saknar behörighet att använda denna funktion!';
            $data['logout'] = TRUE;
        } else {
            $user_data = $this->regterminal_model->get_user_data($card_nr);
            if (count($user_data) === 0 || $user_data['firstname'] === NULL) {
                $data['errormsg'] = 'Kortnumret hittades inte!';
            } else {
                $data['training_sessions'] = $this->regterminal_model->get_training_sessions( $user_data['member_id'], $date, $time );
                $data['user_data'] = $user_data;

                $session_user_data = array(
                    'member_id' => $user_data['member_id'],
                    'instructor' => $user_data['instructor']
                );

                $this->pks_session->store( 'current_user_info', $session_user_data );
            }
        }
        //sleep(7);

        $this->load->view('pages/ajaxresponse', $data);
    }

    public function register_user($member_id, $instructor, $session_id, $time) {
        log_message('debug', __FUNCTION__ . ": Running");

        $session_user_info = $this->pks_session->retrieve( 'current_user_info' );

        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            $data['errormsg'] = 'Du saknar behörighet att använda denna funktion!';
            $data['logout'] = TRUE;
        } else if ( !$session_user_info ) {
            $data['errormsg'] = 'Du måste registrera ditt medlemskort innan du kan använda denna funktion';
        } else {

            if ($instructor === "1" && $session_user_info['instructor'] !== "1") {
                $data['errormsg'] = 'Denna funktion kräver instruktörsrättigheter!';
            } else if ( $session_user_info['member_id'] !== $member_id ) {
                $data['errormsg'] = 'Ogiltigt medlemsnummer!';
            } else {
                $data = Array();
                $this->regterminal_model->register_user_training_session( $member_id, $instructor, $session_id, $time );
            }
        }

        $this->end_registration( FALSE );

        $this->load->view('pages/ajaxresponse', $data);
    }

    public function deregister_user( $member_id, $session_id ) {
        log_message('debug', __FUNCTION__ . ": Running");
        $session_user_info = $this->pks_session->retrieve( 'current_user_info' );

        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            $data['errormsg'] = 'Du saknar behörighet att använda denna funktion!';
            $data['logout'] = TRUE;
        } else if ( !$session_user_info ) {
            $data['errormsg'] = 'Du måste registrera ditt medlemskort innan du kan använda denna funktion';
        } else {

            if ( $session_user_info['member_id'] !== $member_id ) {
                $data['errormsg'] = 'Ogiltigt medlemsnummer!';
            } else {
                $data = Array();
                $this->regterminal_model->deregister_user_training_session( $member_id, $session_id );
            }
        }

        $this->end_registration(FALSE);

        $this->load->view('pages/ajaxresponse', $data);
    }

    /**
     * 
     */
    public function get_session_members_table($session_id) {
        log_message('debug', __FUNCTION__ . ": Running");
        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            json_encode(array()); // Encode an empty array to make datatables happy
        }
        $tabledata = $this->regterminal_model->get_session_members( $session_id, true );
        echo $tabledata;
    }


    public function create_session($member_id, $date, $start_time, $end_time) {
        log_message('debug', __FUNCTION__ . ": running");

        $session_user_info = $this->pks_session->retrieve( 'current_user_info' );

        if ( false ) { //!$this->pks_session->is_logged_in( 'instructor' ) ) {
            $data['errormsg'] = 'Du saknar behörighet att använda denna funktion!';
            $data['logout'] = TRUE;
        } else if ( !$session_user_info ) {
            $data['errormsg'] = 'Du måste registrera ditt medlemskort innan du kan använda denna funktion';
        } else {

            if ( $session_user_info['member_id'] !== $member_id ) {
                $data['errormsg'] = 'Ogiltigt medlemsnummer!';
            } else {

                $start_date_array = date_parse( $date . " " . $start_time );
                $end_date_array = date_parse( $date . " " . $end_time );

                $start_unixtime = mktime($start_date_array['hour'], $start_date_array['minute'], $start_date_array['second'], 0, 0, 0);
                $end_unixtime = mktime($end_date_array['hour'], $end_date_array['minute'], $end_date_array['second'], 0, 0, 0);

                if ($start_unixtime >= $end_unixtime) {
                    $data['errormsg'] = 'Ogiltiga start- och sluttider!';
                } else {

                    $data = array();
                    $this->regterminal_model->create_training_session( $member_id, $date, $start_time, $end_time );
                }
            }
        }

        $this->end_registration(FALSE);

        $this->load->view('pages/ajaxresponse', $data);
    }

    public function end_registration($response = TRUE) {
        log_message('debug', __FUNCTION__ . ": Starting end_registration");
        $this->pks_session->remove( 'current_user_info' );

        if ($response) {
            /* This function doesn't return anything for the moment,
             * just send an empty string to make the front-end happy */
            echo json_encode("");
        }
    }
}