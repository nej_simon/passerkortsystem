<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    private $menu_structure = array(
        array(
            'icon' => '<i class="fa fa-users"></i>',
            'title' => 'Medlemmar',
            'class' => 'members-tab',
            'function' => '/admin/members'
        ),
        array(
            'icon' => '<i class="fa fa-list-alt"></i>',
            'title' => 'Träningspass',
            'class' => 'sessions-tab',
            'function' => '/admin/training_sessions'
        ),
        array(
            'icon' => '<i class="fa fa-calendar"></i>',
            'title' => 'Träningsschema',
            'class' => 'schedule-tab',
            'function' => '/admin/training_schedule'
        ),
        array(
            'icon' => '<i class="fa fa-bar-chart-o"></i>',
            'title' => 'Statistik',
            'class' => 'statistics-tab',
            'function' => '/admin/statistics'
        ),
    );

	public function __construct() {
        parent::__construct();
        log_message( 'debug', __FUNCTION__ . ": Current userdata: " . json_encode( $this->pks_session->retrieve_all() ) );
        
        if ( ! $this->pks_session->is_logged_in( 'admin' ) ) {
            if ( $this->input->is_ajax_request() ) {
                echo "";
            } else {
                redirect('/authentication/admin');
            }
        }

        $this->load->helper('util');
    }

    private function create_menu( $selected ) {
        $output = "<ul class='tablist'>";
        
        foreach ( $this->menu_structure as $item ) {
            $url = site_url( $item['function'] );
            $class = $item['class'] . ( $item['class'] === $selected ? ' active-tab' : '' );
            $output .= "<li class='{$class}'><a href='{$url}'>{$item['icon']}{$item['title']}</a></li>";
        }

        return $output . "</ul>";
    }

    public function index() {
        log_message('debug', __FUNCTION__ . ": Running");

        // Load the members view by default
        $this->members();
	}

    public function members( $member_id = NULL ) {
        log_message('debug', __FUNCTION__ . ": Running");
        
        $header_data = array( 'title' => 'Hantera medlemmar',
                              'menu' => $this->create_menu( 'members-tab' ) );

        $data = array( 'member_id' => $member_id );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/members', $data );
        $this->load->view( 'templates/footer' );
    }

    public function training_sessions() {
        log_message('debug', __FUNCTION__ . ": Running");

        $header_data = array( 'title' => 'Hantera träningspass',
                              'menu' => $this->create_menu( 'sessions-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/training_sessions' );
        $this->load->view( 'templates/footer' );
    }

    public function training_schedule() {
        log_message('debug', __FUNCTION__ . ": Running");
        
        $header_data = array( 'title' => 'Hantera träningsschema',
                              'menu' => $this->create_menu( 'schedule-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/training_schedule' );
        $this->load->view( 'templates/footer' );
    }

    public function statistics() {
        log_message('debug', __FUNCTION__ . ": Running");
        
        $header_data = array( 'title' => 'Statistik',
                              'menu' => $this->create_menu( 'statistics-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/statistics' );
        $this->load->view( 'templates/footer' );
    }

    public function newmember() {
        log_message('debug', __FUNCTION__ . ": Running");

        $header_data = array( 'title' => 'Ny medlem',
                              'menu' => $this->create_menu(  'members-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/newmember' );
        $this->load->view( 'templates/footer' );
    }

    public function new_training_session() {
        log_message('debug', __FUNCTION__ . ": Running");

        $header_data = array( 'title' => 'Nytt träningspass',
                              'menu' => $this->create_menu( 'schedule-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/new_training_session' );
        $this->load->view( 'templates/footer' );
    }

    public function edit_training_session( $session_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');

        $session_data = $this->training_sessions_model->get_session_data( $session_id );
            
        $session_data['session_id'] = $session_id;

        $header_data = array( 'title' => 'Ändra träningspass',
                              'menu' => $this->create_menu( 'sessions-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/edit_training_session', $session_data );
        $this->load->view( 'templates/footer' );
    }

    public function editmember( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $data = array( 'member_id' => $member_id );
        
        $header_data = array( 'title' => 'Ändra medlem',
                              'menu' => $this->create_menu( 'members-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/editmember', $data );
        $this->load->view( 'templates/footer' );
    }

    public function edit_scheduled_session( $schedule_id ) {
        log_message('debug', __FUNCTION__ . ": Running");
        
        $this->load->model('adminUI/training_sessions_model');

        $post_keys = array( "name", "day", "start_time", "end_time" );
        $post_values = array();

        foreach( $post_keys as $key ) {
            $value = $this->input->post( $key );
            if ( $value !== FALSE && $value !== "" )
                $post_values[ $key ] = $value;
        }

        $this->training_sessions_model->edit_scheduled_session( $schedule_id, $post_values );
        
        redirect( 'admin/training_schedule' );
    }

    public function addgrade($member_id) {
        log_message('debug', __FUNCTION__ . ": Running");

        $data = array('member_id' => $member_id);
        
        $header_data = array( 'title' => 'Lägg till grad',
                              'menu' => $this->create_menu(  'members-tab' ) );
        $this->load->view( 'templates/header', $header_data );
        $this->load->view( 'adminUI/addgrade', $data );
        $this->load->view( 'templates/footer' );
    }

	public function get_members() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/members_model');
        $this->members_model->get_members();
	}

    public function get_members_small() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/members_model');
        $this->members_model->get_members_small();       
    }

    public function get_member_data( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/members_model');
        $this->members_model->get_member_data( $member_id );
    }

    public function get_training_sessions( $member_id = NULL ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_training_sessions( $member_id );
    }

    public function get_training_session_count( $member_id, $_group = TRUE, $date = NULL ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $group = filter_var( $_group, FILTER_VALIDATE_BOOLEAN );

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_training_session_count( $member_id, $group, $date );
    }

    //Todo: Merge the following functions here and in the model
    
    public function get_non_exported_training_sessions() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_non_exported_training_sessions();
    }

    public function get_special_training_sessions() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_special_training_sessions();
    }
    // ^^

    public function get_schedule_data( $schedule_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_schedule_data( $schedule_id );
    }

    public function get_training_schedule() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->get_training_schedule();
    }

    public function get_session_members($session_id) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $tabledata = $this->training_sessions_model->get_session_members( $session_id );
        echo $tabledata;
    }

    public function get_grades( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model( 'adminUI/members_model' );
        $this->members_model->get_grades( $member_id );
    }

    public function new_member() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/members_model');
        $post_keys = array( "card_nr", "firstname", "lastname", "ssn",
            "email", "phone","street_address","postcode", "city", "instructor",
            "permanent_member", "admin", "password", "payment_period");
        $post_values = array();

        foreach( $post_keys as $key ) {
            $value = $this->input->post( $key );
            if ( $value !== FALSE && $value !== "" )
                $post_values[ $key ] = $value;
        }

        if ( $_FILES['photo']['error'] === UPLOAD_ERR_NO_FILE ) {
            $member_id = $this->members_model->write_member_to_db( $post_values );
        } else if ( $_FILES['photo']['error'] ) {
            die( 'Systemet misslyckades att ladda upp fotot och den patetiska ursäkten som gavs var: '
                . $upload_errors[$_FILES['photo']['error']] );
        } else {
            $member_id = $this->members_model->write_member_to_db( $post_values, $_FILES );
        }

        redirect( 'admin/members/' . $member_id );

    }

    public function save_training_session() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $post_keys = array( "name", "day", "start_time", "end_time", "psdate" );
        $post_values = array();

        foreach( $post_keys as $key ) {
            $value = $this->input->post( $key );
            if ( $value !== FALSE && $value !== "" )
                $post_values[ $key ] = $value;
        }

        $this->training_sessions_model->new_training_session( $post_values );

        redirect( 'admin/training_schedule' );
        
    }

    public function edit_member( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running" );

        $this->load->model('adminUI/members_model');
        $post_keys = array( "card_nr", "firstname", "lastname", "ssn",
            "email", "phone", "street_address","postcode","city", "instructor",
            "permanent_member", "admin", "password", "payment_period");
        $post_values = array();

        foreach( $post_keys as $key ) {
            $value = $this->input->post( $key );
            if ( $value !== FALSE && $value !== "" )
                $post_values[ $key ] = $value;
        }

        if ( $_FILES['photo']['error'] === UPLOAD_ERR_NO_FILE ) {
            $this->members_model->update_member( $member_id, $post_values );
        } else if ( $_FILES['photo']['error'] ) {
            die( 'Systemet misslyckades att ladda upp fotot och den patetiska ursäkten som gavs var: '
                . $upload_errors[$_FILES['photo']['error']] );
        } else {
            $this->members_model->update_member( $member_id, $post_values, $_FILES );
        }

        redirect( 'admin/members/' . $member_id );
        
    }

    public function save_new_grade( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $grade = $this->input->post("grades");
        $date = $this->input->post("date");
        $this->load->model('adminUI/members_model');
        $this->members_model->add_grade_db( $member_id, $grade, $date );

        redirect( 'admin/addgrade/' . $member_id );
    }

    public function add_member_to_training_session( $member_id, $session_id, $instructor ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->add_member_to_training_session( $member_id, $session_id, $instructor );
        redirect( 'admin/edit_training_session/' . $session_id );
    }

    public function delete_member( $member_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model( 'adminUI/members_model' );
        $this->members_model->delete_member( $member_id) ;

        redirect( 'admin/members' );
    }

    public function delete_training_session($session_id) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->delete_training_session($session_id);
        
        redirect( 'admin/training_sessions' );
    }

    public function delete_scheduled_session( $schedule_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->delete_scheduled_session( $schedule_id );
        
        redirect( 'admin/training_schedule' );
    }

    public function deletegrade($grade_id,$member_id) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model( 'adminUI/members_model' );
        $this->members_model->delete_grade( $grade_id );

        redirect( 'admin/addgrade/' . $member_id);
    }

    public function delete_member_from_training_session( $member_id, $session_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->delete_member_from_training_session( $member_id, $session_id) ;
        redirect( 'admin/edit_training_session/' . $session_id );
    }

    public function updateexported( $session_id ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->model('adminUI/training_sessions_model');
        $this->training_sessions_model->update_exported($session_id);
        
        redirect( 'admin/training_sessions' );
    }

    public function generate_card( $member_id ) {
        $this->load->model( 'adminUI/members_model' );
        $this->members_model->generate_card( $member_id );
    }
}
