<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Various functions for authentication
 */
class Authentication extends CI_Controller {

    /**
     * Construct a new instance
     */
    public function __construct() {

        parent::__construct();
        $this->load->model( 'authentication_model' );
        $this->load->library( 'form_validation' );
        
    }

    /**
     * Load the regterminal authentication view
     * @return null
     */
    public function index() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->load->view( 'pages/regterminalauth_view' );
        $this->load->view( 'templates/footer' );
    }

    public function admin() {
        log_message('debug', __FUNCTION__ . ": Running");

        if ( $this->pks_session->is_logged_in( 'admin' ) ) {
            redirect( "/admin" );
        }
        
        $this->load->view( 'pages/adminauth_view' );
        $this->load->view( 'templates/footer' );
    }

    /**
     * Log in a user by username and password
     * @return null
     */
    public function login_user() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->form_validation->set_rules( 'member_id', 'Member ID', 'trim|required|xss_clean' );
        $this->form_validation->set_rules( 'password', 'Password', 'trim|required|xss_clean|callback_verify_user' );

        if ( $this->form_validation->run() == FALSE ) {
            $data['errormsg'] = validation_errors();
            $this->load->view( 'pages/ajaxresponse', $data );
        } else {
            $data['login'] = TRUE;
            $this->load->view( 'pages/ajaxresponse', $data );
        }
    }


    /**
     * Verify a user from the database when logging in
     * @param  string $password
     * @return Boolean true if the account was verified, false otherwise
     */
    public function verify_user( $password ) {
        log_message('debug', __FUNCTION__ . ": Running");

        //Field validation succeeded.  Validate against database
        $member_id = $this->input->post( 'member_id' );
        $service = $this->input->post( 'service' );
        $password = $this->input->post( 'password' );
        $result = $this->authentication_model->verify_user( $member_id, $password, $service );
        
        if ($result) {
            unset($result['password']); // Don't store the password in the session
            $this->pks_session->store( 'login_info', $result );
            return TRUE;
        } else {
            $this->form_validation->set_message( 'verify_user', 'Ogiltiga inloggningsuppgifter.' );
            return FALSE;
        }
    }
    
    /**
     * Log in a user by card
     * @return null
     */
    public function login_user_by_card( ) {
        log_message('debug', __FUNCTION__ . ": Running");

        $card_nr = $this->input->post( 'card_nr' );
        $service = $this->input->post( 'service' );
        $result = $this->authentication_model->verify_card( $card_nr, $service );

        if ($result) {
            $this->pks_session->store( 'login_info', $result );
            $data['login'] = TRUE;
            $this->load->view( 'pages/ajaxresponse', $data );
        } else {
            $data['errormsg'] = 'Kortnumret hittades inte eller så saknar du behörighet!';
            $this->load->view( 'pages/ajaxresponse', $data );
        }
    }

    /**
     * Log out the current user
     * @return null
     * @todo Return to the correct login page
     */
    public function logout() {
        log_message('debug', __FUNCTION__ . ": Running");

        $this->pks_session->logout();
        redirect( "/authentication/admin" );
    }

}
?>
