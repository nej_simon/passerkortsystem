<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Passhasher extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('phpass');
        
    }

    public function index() {
        echo "Usage: " . site_url("passhasher/hash/foo");
    }
    
    public function hash($password) {
        echo $this->phpass->hash($password);
    }
}
