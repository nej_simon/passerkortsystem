<?php
if ( !defined( 'BASEPATH' ) )
	exit( 'No direct script access allowed' );

class Members_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library( 'Datatables' );
		$this->load->library( 'phpass' );
		$this->load->helper( 'util' );
	}

	public function get_members() {
		$this->datatables->select( 'member_id,firstname,lastname,ssn,phone,payment_period' )->from( 'members' );
		echo $this->datatables->generate();
	}

	public function get_members_small() {
		$this->datatables->select( 'member_id,firstname,lastname,ssn' )->unset_column( 'member_id' )->from( 'members' )->group_by( 'member_id' );
		echo $this->datatables->generate();
	}

	public function get_member_data( $member_id ) {
		$this->db->select( 'card_nr,firstname,lastname,ssn,email,phone,street_address,postcode,city,photo,instructor,permanent_member,admin,payment_period,max(grade) as grade,date as grade_date' )
			->from( 'members' )
			->join( 'grading', 'grading.member_id_fk = members.member_id', 'left' )
			->where( 'member_id', $member_id );
		$query = $this->db->get()->row_array();
		
		if ( $query['photo'] ) {
			if ( $photo_thumb = get_photo_thumbnail( $query['photo'] ) )
				$query['photo_thumb'] = $photo_thumb;
			else
				$query['photo'] = NULL;
		}

		echo json_encode( $query );
	}

	public function get_grades( $member_id ) {
		$this->datatables->select( 'grade, date, grade_id' )->from( 'members' )->join( 'grading', 'members.member_id = grading.member_id_fk' )->where( 'member_id', $member_id );
		echo $this->datatables->generate();
	}

	public function update_member( $member_id, $member_data, $files = NULL ) {

		if ( $files ) {
			$member_data[ 'photo' ] = handle_photo_upload( $files[ 'photo' ] );
		}
		
		// Hash the password before storing it in the db
		if ( isset( $member_data['password'] ) )
			$member_data['password'] = $this->phpass->hash( $member_data['password'] );

		$this->db->where( 'member_id', $member_id )
			->update( 'members', $member_data );
	}
	
	public function delete_member( $id ) {
		$this->db->delete( 'member_training_rel', array(
			'member_id_fk' => $id 
		));
		$this->db->delete( 'members', array(
			'member_id' => $id 
		));
	}
	
	public function delete_grade( $grade_id ) {
		$this->db->delete( 'grading', array(
			 'grade_id' => $grade_id 
		) );
	}

	/**
	 * [write_member_to_db description]
	 * @param  [type] $member_data [description]
	 * @param  [type] $files       [description]
	 * @todo   Don't just die() on errors.
	 * @return [type]              [description]
	 */
	public function write_member_to_db( $member_data, $files = NULL ) {
		log_message('debug', __FUNCTION__ . ": Running");

		if ( $files ) {
			$member_data['photo'] = handle_photo_upload( $_FILES["photo"] );
		}
		
		// Hash the password before storing it in the db
		if ( isset( $member_data['password'] ) )
			$member_data['password'] = $this->phpass->hash( $member_data['password'] );

		$this->db->insert( 'members', $member_data );

		return $this->db->insert_id();
	}

	public function add_grade_db( $member_id, $grade, $date ) {
		$data = array(
			'member_id_fk' => $member_id,
			'grade' => $grade,
			'date' => $date 
		);
		$this->db->insert( 'grading', $data );
	}

	public function generate_card( $member_id ) {

		$this->load->library( 'FPDI' );
		$fpdi = $this->fpdi;

		$card_data = $this->db->select( 'member_id, firstname, lastname, photo' )->from( 'members' )->where( 'member_id', $member_id )->get()->row_array();
		if ( ! $card_data )
			throw Exception( "Could not generate card, user not found!" );

		// FPDF want windows-1252 encoding
		if ( function_exists( 'iconv' ) ) {
			$firstname = iconv( 'UTF-8', 'windows-1252', $card_data[ 'firstname' ] );
			$lastname = iconv( 'UTF-8', 'windows-1252', $card_data[ 'lastname' ] );
		} else {
			// Fall back to utf8_decode if the iconv extension is not installed
			log_message('error', __FUNCTION__ . ": iconv extension not found, falling back to utf8_decode! Install iconv if text output look screwy.");
			$firstname = utf8_decode( $card_data[ 'firstname' ] );
			$lastname = utf8_decode( $card_data[ 'lastname' ] );
		}

		if ( $card_data[ 'photo' ] && $photo_fn = get_photo_thumbnail( $card_data[ 'photo' ] ) ) {
			$photo = PHOTO_UPLOAD_DIR . '/' . $photo_fn;
		} else {
			$photo = PHOTO_DEFAULT;
		}

		$fpdi->setSourceFile( CARD_TEMPLATE );
		$template_id = $fpdi->importPage( 1 ); // The template isn't expected to have more than 1 page
		
		$fpdi->addPage( 'L', array( 85.60, 53.98 ) ); // The size of a regular smart card
		
		//TODO: Check dimensions
		$fpdi->useTemplate( $template_id );

		$fpdi->SetFont( 'Arial', '', 14);
		$fpdi->Text( 30, 30, $firstname );
		$fpdi->Text( 30, 36, $lastname );

		$fpdi->Image( $photo, 4, 20, 24 );

		// Send the generated pdf to the browser
		$fpdi->Output( 'output.pdf', 'I' );

	}
}
