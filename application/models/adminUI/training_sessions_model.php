<?php
if ( !defined( 'BASEPATH' ) )
	exit( 'No direct script access allowed' );

class training_sessions_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library( 'Datatables' );
	}

	public function get_training_sessions( $member_id ) {
		$query = $this->datatables->select( "schedule_id,session_id,name,date, WEEKDAY( date ) + 1 as day,training_sessions.start_time as start_time,training_sessions.end_time as end_time, exported" )
			->unset_column( 'schedule_id' )
			->unset_column( 'session_id' )
			->from( 'training_sessions' )
			->join( 'training_schedule', 'schedule_id_fk = schedule_id', 'left' )
			->join( 'member_training_rel', 'training_session_id_fk = session_id', 'left' )
			->join( 'members', 'member_id_fk = member_id', 'left' )
			->group_by( 'session_id' );

		if ( $member_id ) {
			$query->where( 'member_id', $member_id );
		}

		echo $this->datatables->generate();
		
	}

	public function get_non_exported_training_sessions() {
		
		$this->datatables->select( 'schedule_id,session_id,date,exported,name,WEEKDAY( date ) + 1 as day,training_sessions.start_time,training_sessions.end_time' )
			->unset_column( 'schedule_id' )
			->unset_column( 'session_id' )
			->from( 'training_sessions' )
			->join( 'training_schedule', 'schedule_id_fk = schedule_id' )
			->join( 'member_training_rel', 'training_session_id_fk = session_id' )
			->join( 'members', 'member_id_fk = member_id' )
			->where( 'exported', 0 )
			->group_by( 'session_id' );
		echo $this->datatables->generate();
		
	}

	public function get_special_training_sessions() {
		
		$this->datatables->select( 'schedule_id,session_id,psdate as date,exported,name,day,training_sessions.start_time,training_sessions.end_time' )
			->unset_column( 'schedule_id' )
			->unset_column( 'session_id' )
			->from( 'training_sessions' )
			->join( 'training_schedule', 'schedule_id_fk = schedule_id' )
			->where( 'psdate IS NOT NULL' )
			->group_by( 'session_id' );
		echo $this->datatables->generate();
		
	}

	public function get_training_session_count( $member_id, $group, $date ) {
		$query = $this->db->select( 'COUNT(*) as count' )
			->from( 'member_training_rel' )
			->join( 'training_sessions', 'training_session_id_fk = session_id' )
			->join( 'training_schedule', 'schedule_id = schedule_id_fk', 'left' )
			->where( 'member_id_fk', $member_id );

		if ( $date ) {
			$query->where( 'date >=', $date );
		}

		if ( $group ) {
			$query->select( 'name' );
			$query->group_by( 'schedule_id_fk' );
		}

		echo json_encode( $query->get()->result_array() );
	}

	public function get_training_schedule() {
		$this->datatables->select( 'schedule_id,name,day,start_time,end_time' )
			->unset_column( 'schedule_id' )
			->from( 'training_schedule' );
		echo $this->datatables->generate();
	}

	public function get_schedule_data( $schedule_id ) {
		$this->db->select( 'name,day,start_time,end_time' )
			->from( 'training_schedule' )
			->where( 'schedule_id', $schedule_id );
		$query = $this->db->get()->row_array();
		echo json_encode( $query );
	}

	public function get_session_data( $session_id ) {
		$this->db->select( 'name, date' )
			->from( 'training_sessions' )
			->join( 'training_schedule', 'schedule_id_fk = schedule_id', 'left' )
			->where( 'session_id', $session_id );
		$query = $this->db->get()->row_array();

		return $query;
	}

	public function get_session_members( $session_id ) {

        $this->datatables->select('firstname, lastname, max(grade) as grade, ssn, member_training_rel.instructor as instructor')
                ->from('members')
                ->join('grading', 'members.member_id = grading.member_id_fk', 'left')
                ->join('member_training_rel', 'members.member_id = member_training_rel.member_id_fk')
                ->join('training_sessions', 'member_training_rel.training_session_id_fk = training_sessions.session_id')
                ->where('training_sessions.session_id', $session_id)
                ->group_by('member_id');

        return $this->datatables->generate();
    }

	
	public function new_training_session( $training_session_data ) {
		
		$this->db->insert( 'training_schedule', $training_session_data);
		$schedule_id = $this->db->insert_id();
		
		if ( isset( $training_session_data['psdate'] ) ) {

			$rel_data = array(
				'date' => $training_session_data['psdate'],
				'start_time' => $training_session_data['start_time'],
				'end_time' => $training_session_data['end_time'],
				'schedule_id_fk' => $schedule_id 
			);
			
			$this->db->insert( 'training_sessions', $rel_data );
		}
	}
	
	public function edit_scheduled_session( $schedule_id, $data ) {
		
		$this->db->where( 'schedule_id', $schedule_id )
			->update( 'training_schedule', $data );

	}

	public function get_grades( $member_id ) {
		$this->datatables->select( 'grade_id, firstname, lastname, grade, date' )
			->unset_column( 'grade_id' )
			->from( 'members' )
			->join( 'grading', 'members.member_id = grading.member_id_fk', 'left' )
			->where( 'member_id', $member_id );
		echo $this->datatables->generate();
	}

	public function add_member_to_training_session( $member_id, $session_id, $instructor ) {

		$this->db->select( 'start_time' )->from( 'training_sessions' )->where( 'session_id', $session_id );
	 	$query = $this->db->get()->row_array();

		$data = array(
			'member_id_fk' => $member_id,
			'time' => $query['start_time'],
			'training_session_id_fk' => $session_id,
			'instructor' => $instructor 
		);
		$this->db->insert( 'member_training_rel', $data );
	}

	public function delete_member_from_training_session( $member_id, $session_id ) {
		$this->db->delete( 'member_training_rel', array(
			'member_id_fk' => $member_id,
			'training_session_id_fk' => $session_id 
		) );
	}

	public function delete_training_session( $session_id ) {
		$this->db->delete( 'training_sessions', array(
			 'session_id' => $session_id 
		) );
	}

	public function delete_scheduled_session( $schedule_id ) {
		$this->db->delete( 'training_schedule', array(
			 'schedule_id' => $schedule_id 
		) );
	}
	
	public function update_exported( $session_id ) {
		$this->db->set( 'exported', '1', FALSE )
			->where( 'session_id', $session_id )
			->update( 'training_sessions' );
	}
}
