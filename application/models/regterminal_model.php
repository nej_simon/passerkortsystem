<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * TODO: Better check for MySQL errors.
 */

class Regterminal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //TODO: Consolidate the following two functions if possible
    private function create_training_sessions($new_sessions, $date) {

        $row = array();

        for ($i = 0; $i < count($new_sessions); $i++) {
            $row[] = array("schedule_id_fk" => $new_sessions[$i]['schedule_id'],
                "start_time" => $new_sessions[$i]['start_time'],
                "end_time" => $new_sessions[$i]['end_time'],
                "date" => $date);
        }

        $this->db->insert_batch("training_sessions", $row);

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
    }

    private function get_session_info($session_id) {

        $result = $this->db->select('date, start_time, end_time, exported, schedule_id_fk')
                        ->from('training_sessions')
                        ->where('session_id', $session_id)
                        ->get()->row_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode( $result ));

        return $result;
    }

    public function get_session_members($session_id, $use_datatables = false) {

        if ($use_datatables) {
            $this->load->library('Datatables');
            $database = $this->datatables;
        } else {
            $database = $this->db;
        }

        $database->select('firstname, lastname, max(grade) as grade, payment_period,
                    members.instructor as instructor, permanent_member')
                ->from('members')
                ->join('grading', 'members.member_id = grading.member_id_fk', 'left')
                ->join('member_training_rel', 'members.member_id = member_training_rel.member_id_fk')
                ->join('training_sessions', 'member_training_rel.training_session_id_fk = training_sessions.session_id')
                ->where('training_sessions.session_id', $session_id)
                ->group_by('member_id');

        if ($use_datatables) {
            $result = $this->datatables->generate();
        } else {
            $result = $this->db->get()->result_array();
        }

        //log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());

        return $result;
    }

    public function create_training_session($member_id, $date, $start_time, $end_time) {
        $row = array("start_time" => $start_time,
            "end_time" => $end_time,
            "date" => $date);

        $this->db->insert('training_sessions', $row);

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());

        $this->register_user_training_session($member_id, 1, $this->db->insert_id(), $start_time);
    }

    public function get_user_data($card_nr) {

        $result = $this->db->select('member_id, firstname, lastname, photo, max(grade) as grade, payment_period, instructor, permanent_member')
                        ->from('members')
                        ->join('grading', 'members.member_id = grading.member_id_fk', 'left')
                        ->where('card_nr', $card_nr)
                        ->get()->row_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode( $result ));

        return $result;
    }


    public function get_training_sessions($member_id, $date, $time) {

        $date_array = date_parse($date . " " . $time);
        $date_unixtime = mktime(0, 0, 0, $date_array['month'], $date_array['day'], $date_array['year']);
        $day = date('N', $date_unixtime);

        $start_unixtime = mktime($date_array['hour'] - 1, $date_array['minute'], $date_array['second'], 0, 0, 0);
        $end_unixtime = mktime($date_array['hour'] + 1, $date_array['minute'], $date_array['second'], 0, 0, 0);

        $start_time_day = explode('-', date("d-H:i:s", $start_unixtime));
        $end_time_day = explode('-', date("d-H:i:s", $end_unixtime));

        $start_time = $start_time_day[1];

        if ($end_time_day[0] !== $start_time_day[0]) { // New day
            $end_time_components = explode(':', $end_time_day[1]);
            $end_time = $end_time_components[0] + 24 . ':' . $end_time_components[1] . ':' . $end_time_components[2];
        } else {
            $end_time = $end_time_day[1];
        }
        
        $new_sessions = $this->db->select("schedule_id, training_schedule.start_time, training_schedule.end_time")
                        ->from("training_schedule")
                        ->join("training_sessions", "schedule_id = schedule_id_fk", "left")
                        ->where("day", $day)
                        ->where("training_schedule.start_time <", $end_time)
                        ->where("training_schedule.end_time >", $start_time)
                        ->group_by("schedule_id")
                        ->having("MAX(`date`) IS NULL")
                        ->or_having("MAX(`date`) != '" . $date . "'")
                        ->get()->result_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode($new_sessions));

        if (count($new_sessions) != 0) {
            $this->create_training_sessions($new_sessions, $date);
        }

        $sessions = $this->db->select("`session_id`, `name`, `date`, `training_sessions`.`start_time`,
                        `training_sessions`.`end_time`,
                        GROUP_CONCAT((CASE WHEN `member_training_rel`.`instructor` = 1
                            THEN CONCAT(`firstname`, ' ', `lastname`) END) SEPARATOR ', ') AS `instructors`,
                        MAX((CASE WHEN (`members`.`member_id` = " . $member_id . ") THEN 1 ELSE 0 END)) AS `registered`,
                        COUNT(DISTINCT `member_id`) AS `nr_registered`", FALSE) // CI adds backticks in an erroneous way so lets do it ourselves
                        ->from("passerkortsystem.training_sessions")
                        ->join("training_schedule", "schedule_id_fk = schedule_id", "left")
                        ->join("member_training_rel", "training_session_id_fk = session_id", "left")
                        ->join("members", "member_id_fk = member_id", "left")
                        ->where("date = '" . $date . "'")
                        ->where("training_sessions.start_time <", $end_time)
                        ->where("training_sessions.end_time >", $start_time)
                        ->group_by("session_id")
                        ->order_by("training_sessions.start_time", "asc")
                        ->get()->result_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode($sessions));

        return $sessions;
    }

    public function register_user_training_session($member_id, $instructor, $session_id, $time) {

        $member_data = array(
            'member_id_fk' => $member_id,
            'time' => $time,
            'training_session_id_fk' => $session_id,
            'instructor' => $instructor
        );

        $insert_result = $this->db->insert('member_training_rel', $member_data);

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode($insert_result));

        return;
    }

    public function deregister_user_training_session($member_id, $session_id) {

        $session_data = array(
            'member_id_fk' => $member_id,
            'training_session_id_fk' => $session_id
        );

        $this->db->delete('member_training_rel', $session_data);

        $session_info = $this->get_session_info($session_id);
        if ($session_info['schedule_id_fk'] === null) {
            if (count($this->get_session_members($session_id)) === 0) {
                $this->delete_session($session_id);
            }
        }

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
    }

    public function delete_session($session_id) {

        $session_data = array(
            'session_id' => $session_id
        );

        $this->db->delete('training_sessions', $session_data);

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
    }

}
