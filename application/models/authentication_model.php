<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 */
class Authentication_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library( 'phpass' );
    }

    /**
     * Verify a user using username and hashed password from the database
     * @param  string $member_id [description]
     * @param  string $password  [description]
     * @param  string $service   [description]
     * @return mixed             [description]
     */
    public function verify_user( $member_id, $password, $service ) {
        $result = $this->db->select( "member_id, password, instructor, admin" )
                        ->from( "members" )
                        ->where( "member_id", $member_id )
                        ->get()->row_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode( $result ));

        if ( ! $result ) {
            return false;
        } else if ( $this->phpass->check( $password, $result['password'] ) ) {
            if ( $service == "admin" && $result["admin"] == "1" ) {
                return $result;
            } else {
                return false;
            }
        }
    }

    /**
     * verify a user using the card nr
     * @param  string $card_nr [description]
     * @param  string $service [description]
     * @return mixed           [description]
     */
    public function verify_card( $card_nr, $service ) {
        $result = $this->db->select( "member_id, card_nr, instructor, admin" )
                        ->from( "members" )
                        ->where( "card_nr", $card_nr )
                        ->get()->row_array();

        log_message('debug', __FUNCTION__ . ": Ran SQL query: " . $this->db->last_query());
        log_message('debug', __FUNCTION__ . ": Got SQL result: " . json_encode( $result ));
        
        if ( !$result ) {
            return false;
        } else if ( $service == "regterminal" && $result["instructor"] == "1" ) {
            return $result;
        } else {
            return false;
        }
    }

}