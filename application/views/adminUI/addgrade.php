	<script>
	var member_id = <?php echo $member_id; ?>;
	var member_spec = "<?php echo base_url(); ?>index.php/admin/get_member_data" + member_id;

	$(document).ready( function() {
		var date = $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" }).val();

		oTable = $('#table').dataTable( {
                "oLanguage": {
	                "sSearch": "Sök: ",
	                "sInfo": "Visar _START_ till _END_ av _TOTAL_ grader.",
	                "sLengthMenu": "Visar _MENU_ grader per sida",
	                "sInfoFiltered": "Filtrerat från _MAX_ totalt",
	                "sZeroRecords": "Medlemmen har ej graderat!",
	                "sInfoEmpty": "Inga grader funna!",
	                "oPaginate": {
	                    "sFirst": "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
	                    "sLast": "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
	                    "sNext": "<i class='fa fa-chevron-right'></i>",
	                    "sPrevious": "<i class='fa fa-chevron-left'></i>"
	                },
	            },
                "sDom": 'lrtip',
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "bLengthChange": false,
                "sAjaxSource": '<?php echo base_url(); ?>index.php/admin/get_grades/' + member_id,
                "aoColumnDefs": [
                    { "aTargets": [0],
                        "mRender": function( number ) {
                            grade = helpers.nrToKarateGrade( number );
                            if ( grade )
                                return grade;
                            else
                                return 'Ej graderad';
                        }
                    },
                    { "aTargets": [2],
                        "bVisible": false
                    } ],
                "bJQueryUI": false, 
                "aaSorting": [[0, 'asc']],
                "sServerMethod": "POST"
            });

		$('#table').on('click', 'tr', function(event) {
			if ( ! oTable.fnGetData(this) )
				return;

			$('.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');

			var grade_id = oTable.fnGetData(this)[2];

			$('#grade_delete_button').attr( 'href', "<?php echo site_url('/admin/deletegrade'); ?>/" + grade_id + "/" + member_id );
		});

		var gradeOptions = $('#grade_options');
		var grade = 1;
		while ( gradeString = helpers.nrToKarateGrade( grade ) ) {
			gradeOptions.append("<option value='" + grade + "'>" + gradeString + "</option>");
			grade++;
		}
	});
	</script>
	<div id="left_container">
		<a class="button" href="<?php echo site_url('/admin/members/' . $member_id ); ?>">Tillbaka</a>
		<div id="table_container">
			<table id="table" cellpadding="2" cellspacing="1" class="display">
				<thead>
					<tr> 
						<th>Grad</th>
						<th>Datum</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2" class="datatables_empty"><font color="red">Laddar grader från databasen..</font></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="right_container">
		<div id="grade_form"> 
			<form action="<?php echo base_url(); ?>index.php/admin/save_new_grade/<?php echo $member_id ?>" method="post">
				<select name="grades" id="grade_options"></select>
				<p>Datum: <input type="text" id="datepicker" name="date"></p>
				<input class="button" type="submit" value="Lägg till">
			</form>
		</div>
		<a class='button' id="grade_delete_button" href='#'>Radera grad</a>
		</div>
	</div>