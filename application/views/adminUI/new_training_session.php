		<script>
		$(document).ready(function() {
			$( "#form" ).validate({
				rules: {
					name: {
						required: true,
						minlength: 2,
						maxlength: 50,
						specialCharacterValidation: true
					}
				},
				messages: {
					name: {
						required: "Namn på träningspass är ett krav.",
						minlength: $.format("Minst två tecken långt."),
						maxlength: $.format("Max tjugo tecken långt.")
					}
				}
			});

			$.validator.addMethod("specialCharacterValidation",
				function(value, element) {
						return /^[A-Öa-ö\d=#$%@_ -]+$/.test(value);
					},
					"Namnet kan bara bestå av vanliga tecken.");

			var date = $( "#special-date" ).datepicker({ dateFormat: "yy-mm-dd" }).val();

			$( "#special-session" ).on("change", function() {
				$( "#special-date" ).attr('disabled', ! $(this).is(':checked') )
			});
		});
		</script>
		<div id="member_form">
			<h2> Skapa nytt träningspass </h2>
			<form id="form" name="register" action="<?php echo site_url('/admin/save_training_session'); ?>" method="post">
				<table>
					<tr>
						<td>Namn</td>
						<td><input type="text" name="name"></td>
					</tr>
					<tr>
						<td>Dag</td>
						<td>
							<select name="day">
								<option value="1">Måndag</option>
								<option value="2">Tisdag</option>
								<option value="3">Onsdag</option>
								<option value="4">Torsdag</option>
								<option value="5">Fredag</option>
								<option value="6">Lördag</option>
								<option value="7">Söndag</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Starttid</td>
						<td>
							<select name="start_time">
								<?php echo print_time_options(); ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Sluttid</td>
						<td>
							<select name="end_time">
								<?php echo print_time_options(); ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Specialpass</td>
						<td><input type="checkbox" id="special-session" name="special"></td>
					</tr>
					<tr>
						<td>Datum för specialpass.</td>
						<td><input type="text" disabled id="special-date" name="psdate"></td>
					</tr>
					<tr>
						<td><input class="button" type="submit" value="Spara"></td>
						<td><a class="button" href="<?php echo site_url('/admin/training_schedule'); ?>">Avbryt </a></td>
					</tr>
				<table>
			</form>

		</div>
