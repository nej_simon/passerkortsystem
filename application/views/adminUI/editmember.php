		<script>
		var member_id = <?php echo $member_id;?>;
		var member_spec = "<?php echo base_url(); ?>index.php/admin/get_member_data/" + member_id;
		var inputEnabled = false;
		var inputTimeout = 750;
		var currentInput = '';
		var timeout = false;

		/**
         * Handle input from the reader
         * @param  {[type]} e [description]
         * @return {[type]}   [description]
         */
        function handleInput( e ) {
            if (inputEnabled) {

                var currentChar = String.fromCharCode( e.which );

                if ( !timeout ) {
                    timeout = window.setTimeout( function() {
                        $("#card_nr_hidden").val( currentInput );
						$("#card_nr").text( currentInput );
                        toggleReader();
                        timeout = false;
                        currentInput = '';
                    }, inputTimeout );
                }
                if ( currentChar === '\r' ) {
                    return;
                } else {
                    currentInput += currentChar;
                }
            	return false;
            }
        }

        function toggleReader() {
        	$('#read-card-page').toggle();
        	inputEnabled = ! inputEnabled;
        }

		$(document).ready( function() {
			var date = $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" }).val();

			$.ajax({
				type : 'POST',
				dataType: 'json',
				url : member_spec,
				success : function( spec_member_data ) {

					if ( ! spec_member_data.card_nr ) {
						spec_member_data.card_nr = "Inget registrerat";
					}
					$("#card_nr_hidden").val( spec_member_data.card_nr );
					$("#card_nr").text( spec_member_data.card_nr );
					$("#firstname").val( spec_member_data.firstname );
					$("#lastname").val( spec_member_data.lastname );
					$("#ssn").val( spec_member_data.ssn );
					$("#email").val( spec_member_data.email );
					$("#phone").val( spec_member_data.phone );
					$("#street_address").val( spec_member_data.street_address );
					$("#postcode").val( spec_member_data.postcode );
					$("#city").val( spec_member_data.city );
					$("#instructor").val( spec_member_data.instructor );
					$("#permanent_member").val( spec_member_data.permanent_member );
					$("#admin").val( spec_member_data.admin );
					$("#datepicker").val( spec_member_data.payment_period );
				}
			});

			$('#edit-card, #read-card-page .button').on( "click", toggleReader );

			$(document).on("keypress", handleInput);
		});
		</script>
		
		<!-- Page displaying a modal dialog -->
        <div id="read-card-page" class="fullscreen_pages">
            <div class="read-card">
                <h2>Läs in kortet nu..</h2>
                <img src='<?php echo base_url("assets/pictures/card.svg") ?>'>
                <a class="button" href="#">Avbryt</a>
            </div>
        </div>

		<div id="member_form">
			<div id="heading">
				<h2> Redigera medlem </h2>
			</div>
			<br>
			<form id="form" name="register" action="<?php echo base_url(); ?>index.php/admin/edit_member/<?php echo $member_id ?>"  method="post"  enctype="multipart/form-data">
				<table class="form-table">
					<tr>
						<th scope="row">Kortnr</th>
						<td>
							<span id="card_nr"></span><input id="card_nr_hidden" type="hidden" name="card_nr"><a href="#" id="edit-card"><i class="fa fa-pencil-square-o"></i></a>
						</td>
					</tr>
					<tr>
						<th scope="row">Namn</th>
						<td><input id="firstname" type="text" name="firstname"></td>
					</tr>
					<tr>
						<th scope="row">Efternamn</th>
						<td><input id="lastname" type="text" name="lastname"></td>
					</tr>
					<tr>
						<th scope="row">Personnummer</th>
						<td><input id="ssn" type="text" name="ssn"></td>
					</tr>
					<tr>
						<th scope="row">Email</th>
						<td><input id="email" type="text" name="email"></td>
					</tr>
					<tr>
						<th scope="row">Telefon</th>
						<td><input id="phone" type="text" name="phone"></td>
					</tr>
					<tr>
						<th scope="row">Adress</th>
						<td><input id="street_address" type="text" name="street_address"></td>
					</tr>
					<tr>
						<th scope="row">Postnummer</th>
						<td><input id="postcode" type="text" name="postcode"></td>
					</tr>
					<tr>
						<th scope="row">Stad</th>
						<td><input id="city" type="text" name="city"></td>
					</tr>
					<tr>
						<th scope="row">Nytt Lösenord</th>
						<td><input type="password" name="password"></td>
					</tr>
					<tr>
						<!-- TODO: Visa gammalt foto här om något -->
						<th scope="row">Foto</th>
						<td><input type="file" name="photo"></td>
					<tr>
						<th scope="row">Instruktör</th>
						<td>
							<select id="instructor" name="instructor">
								<option value="1">Ja</option>
								<option value="0">Nej</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Permanent medlem</th>
						<td>
							<select id="permanent_member" name="permanent_member">
								<option value="1">Ja</option>
								<option value="0">Nej</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Administratör</th>
						<td>
							<select id="admin" name="admin">
								<option value="1">Ja</option>
								<option value="0">Nej</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Avgift betalad till</th>
						<td><input type="text" id="datepicker" name="payment_period"></td>
					</tr>
					<tr>
						<td>
							<a class="button" href="<?php echo site_url('/admin/members'); ?>">Avbryt</a>
						</td>
						<td>
							<input class="button" type="submit" value="Spara">
						</td>
					</tr>
				</table>
			</form>
		</div>
