<?php
//TODO: Gradering.
?>

        <script>
        var memberData = [];
        var currentMember = <?php echo ( $member_id ? $member_id : -1 ) ?>;        
        var memberTable;
        var gradeTable;
        var sessionTable;

        function switchTab( caller, newPage ) {
            $( '.tab-selected' ).removeClass('tab-selected');
            $( caller ).parent().addClass('tab-selected');
            $( '#spec_box .visible' ).hide().removeClass('visible');
            $( newPage ).show().addClass('visible');
        }

        function writebox( ) {

            switchTab( this, '#member-info' );

            //TODO: This should idealy not run again on a tab switch.

            if ( memberData[currentMember].photo ) {
                var image = $('<img>').attr( 'src', '<?php echo base_url() ?>/assets/uploads/member_photos/' + memberData[currentMember].photo_thumb );
                var link = $('<a></a>')
                    .attr('href', '<?php echo base_url() ?>/assets/uploads/member_photos/' + memberData[currentMember].photo )
                    .attr('title', memberData[currentMember].firstname + " " + memberData[currentMember].lastname)
                    .append( image )
                    .slimbox( { overlayFadeDuration: 1, resizeDuration: 1, imageFadeDuration: 1, captionAnimationDuration: 1 } );
                $('#member-photo').html( link );
            } else
                $('#member-photo').html( '<em>Foto saknas!</em>' );

            $('#member-name').text( memberData[currentMember].firstname + " " + memberData[currentMember].lastname );
            $('#member-nr').text( currentMember );
            $('#member-grade').text( memberData[currentMember].karateGrade );
            $('#member-type').text( memberData[currentMember].type );
            $('#member-phone').text( memberData[currentMember].phone );
            $('#member-email').text( memberData[currentMember].email );
            $('#member-street').text( memberData[currentMember].street_address );
            $('#member-postcode').text( memberData[currentMember].postcode );
            $('#member-city').text( memberData[currentMember].city );

            $('#edit_member_button').attr('href', "<?php echo site_url('/admin/editmember'); ?>/" + currentMember );
            $('#create_membercard_button').attr('href', "<?php echo site_url('/admin/generate_card'); ?>/" + currentMember );

            return false;

        }

        //TODO: Fix
        function writeboxGrade( ) {

            switchTab( this, '#grade-info' );

            if ( ! gradeTable ) {
                initGradeTable();
            } else {
                gradeTable.fnReloadAjax( '<?php echo base_url(); ?>index.php/admin/get_grades/' + currentMember );
            }

            $('#edit_grade_button').attr('href', "<?php echo site_url('/admin/addgrade'); ?>/" + currentMember );

            return false;
        }

        function getSessionCount() {

            $.ajax({
                type : 'POST',
                dataType: 'json',
                url : "<?php echo base_url(); ?>index.php/admin/get_training_session_count/" + currentMember + "/false/" + $('#startdate').val(),
                success : function( data ) {
                    $('#training_session-history .session-count strong').text( 'Antal: ' + data[0].count );
                }
            });

            return false;
        }

        function getMemberData( member ) {
            $.ajax({
                type : 'POST',
                dataType: 'json',
                url : "<?php echo base_url(); ?>index.php/admin/get_member_data/" + member,
                success : function( spec_member_data ) {
                    memberData[currentMember] = spec_member_data;
                    
                    if ( spec_member_data.instructor === "1" ) {
                        memberData[currentMember].type = "Instruktör";
                    }
                    else {
                        memberData[currentMember].type = "Medlem";
                    }

                    if ( memberData[currentMember].grade ) {
                        memberData[currentMember].karateGrade = helpers.nrToKarateGrade( memberData[currentMember].grade );
                    } else {
                        memberData[currentMember].karateGrade = "Ingen";
                    }
                    
                    $('#spec_box .inactive-overlay').hide();

                    if ( memberData[currentMember].grade_date ) {
                        addLink = $('<a href="#"></a>').on("click", function() {
                            $('#startdate').val( memberData[currentMember].grade_date );
                        }).text( memberData[currentMember].grade_date );
                        $('#training_session-history .grade-date').html( addLink );
                    }
                    else
                        $('#training_session-history .grade-date').text( "Inget" );

                    $( '#member_spec_menu .tab-selected a' ).click();
                }
            });
        }

        function writeboxtraining_sessions() {

            switchTab( this, '#all-training_sessions' );

            if ( ! sessionTable ) {
                initSessionTable();
            } else {
                sessionTable.fnReloadAjax( '<?php echo base_url(); ?>index.php/admin/get_training_sessions/' + currentMember );
            }

            return false;
        }

        function writeboxHistory() {

            switchTab( this, '#training_session-history' );

            if ( memberData[currentMember].grade_date ) {
                addLink = $('<a href="#"></a>').on("click", function() {
                    $('#startdate').val( memberData[currentMember].grade_date );
                    return false;
                }).text( memberData[currentMember].grade_date );
                $('#training_session-history .grade-date').html( addLink );
            }
            else
                $('#training_session-history .grade-date').text( "Inget" );
            
            return false;
        }

        function initGradeTable() {
            gradeTable = $('#grade-table').dataTable( {
                oLanguage: { sZeroRecords: "Medlemmen har ej graderat." },
                sDom: 't', // Only display the table, nothing else
                iDisplayLength: -1, // Display all grades for now
                bLengthChange: false,
                bInfo: false,
                bPaginate: false,
                bFilter: false,
                sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_grades/' + currentMember,
                aoColumnDefs: [
                    { aTargets: [0],
                        mRender: function( number ) {
                            grade = helpers.nrToKarateGrade( number );
                            if ( grade )
                                return grade;
                            else
                                return 'Ej graderad';
                        }
                    },
                    { aTargets: [2],
                        bVisible: false
                    } ],
                bJQueryUI: false, 
                aaSorting: [[0, 'asc']],
                sServerMethod: "POST"
            });
        }

        function initSessionTable() {
            sessionTable = $('#session-table').dataTable( {
                oLanguage: {
                    sZeroRecords: "Medlemmen har ej varit på några pass.",
                    oPaginate: {
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
                    }
                },
                sDom: 'tp',
                iDisplayLength: 8,
                bLengthChange: false,
                bInfo: false,
                bPaginate: true,
                bFilter: false,
                sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_training_sessions/' + currentMember,
                aoColumnDefs: [
                {   aTargets: [ 2, 3, 4, 5 ],
                    bVisible: false

                } ],
                bJQueryUI: false, 
                aaSorting: [[0, 'asc']],
                sServerMethod: "POST"
            });
        }


        function initMemberTable() {
            memberTable = $('#member-table').dataTable( {
                oLanguage: {
                    sSearch: "Sök: ",
                    sInfo: "Visar _START_ till _END_ av _TOTAL_ medlemmar.",
                    sLengthMenu: "Visar _MENU_ medlemmar per sida",
                    sInfoFiltered: "Filtrerat från _MAX_ totalt",
                    sZeroRecords: "Inga medlemmar funna.",
                    sInfoEmpty: "Inga medlemmar funna. ",
                    oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
                    }
                },
                bServerSide: true,
                sDom: 'lfr<"table-inner-wrap"t>ip',
                sPaginationType: "full_numbers",
                iDisplayLength: 10,
                bLengthChange: false,
                sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_members',
                aoColumns: [
                { mData: "member_id" },
                { mData: "firstname" },
                { mData: "lastname" },
                { mData: "ssn" },
                { mData: "phone"},
                { mData: "payment_period"} ],
                bJQueryUI: false, 
                aaSorting: [[0, 'asc']],
                sServerMethod: "POST"
            });
            
        }
        
        /* DataTables */
        $(document).ready(function() {

            $('#startdate').datepicker({ dateFormat: "yy-mm-dd" });

            initMemberTable();

            $('#member-table').on('click', 'tr', function(event) {
                if ( ! memberTable.fnGetData(this) )
                    return;

                currentMember = memberTable.fnGetData(this).member_id;

                $('#member-table .row_selected').removeClass('row_selected');
                $(this).addClass('row_selected');

                if ( ! memberData[currentMember] ) {
                    getMemberData(currentMember);
                } else {

                    // If we didn't have to get new data via ajax just reload the tab
                    $( '#member_spec_menu .tab-selected a' ).click();
                }

            });

            $('#tab-memberinfo').on( "click", writebox );
            $('#tab-gradeinfo').on( "click", writeboxGrade );
            $('#tab-stats').on( "click", writeboxtraining_sessions );
            $('#tab-history').on( "click", writeboxHistory );

            $('#get-session-count').on( "click", getSessionCount );

            $('#delete_member_button').on('click', function() {
                openModal(
                    'Ta bort medlem?',
                    'Vill du ta bort <strong>' + memberData[currentMember].firstname + " " + memberData[currentMember].lastname + "</strong>? Denna åtgärd går inte att ångra!",
                    'confirm',
                    function() { window.location.href = "<?php echo site_url('/admin/delete_member'); ?>/" + currentMember; },
                    closeModal
                );
            });

            if ( currentMember >= 0 )
                getMemberData(currentMember);

        });
        </script>
            <div id="left_container" class="sevencol first">
                <div class="table-outer-wrap section-wrap">
                    <div class="table-header"><h2>Medlemmar</h2></div>
                    <table id="member-table" cellpadding="2" cellspacing="1" class="display">
                        <thead>
                            <tr> 
                                <th>Nr</th>
                                <th>Förnamn</th>
                                <th>Efternamn</th>
                                <th width="20%">personnummer</th>
                                <th>Telefon</th>
                                <th>Betalat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5" class="datatables_empty"><br><font color="red">Laddar medlemmar från databas..</font><br><br><br>
                                   <img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'>
                               </td>
                           </tr>
                       </tbody>
                    </table>
                    <div class="button-field">
                        <hr>
                        <a class="button" href="<?php echo site_url('/admin/newmember'); ?>"><i class="fa fa-plus-square-o"></i><span class="button-text">Ny medlem</span></a>
                    </div>
                </div>
            </div>
            <div id="right_container" class="fivecol last">
                <div class="spec-wrap section-wrap">
                    <div id="spec_box" class="info-box">
                        <div class="inactive-overlay"></div>
                        <div id="member_spec_menu">
                            <ul class="tab_spec_list">
                                <li class="tab-selected"><a id="tab-memberinfo" href="#">Medlem</a></li><li><a id="tab-gradeinfo" href="#">Grader</a></li><li><a id="tab-stats" href="#">Aktivitet</a></li><li><a id="tab-history" href="#">Statistik</a></li>
                            </ul>
                            <div class="tab-bottom-overlay"></div>
                        </div>
                        <div class="spec-outer-wrap">

                            <div id="member-info" class="visible" >
                                <div class="spec-inner-wrap">
                                    <div id="member-photo"><em>Foto saknas!</em></div>
                                    <h2 id="member-name">Ingen medlem vald</h2>
                                    <table>
                                        <tr>
                                            <th scope="row">Typ</th>
                                            <td id="member-type"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Medlemsnr</th>
                                            <td id="member-nr"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Grad</th>
                                            <td id="member-grade"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Telefon</th>
                                            <td id="member-phone"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">E-post</th>
                                            <td id="member-email"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Gatuaress</th>
                                            <td id="member-street"></td>
                                        </tr> 
                                        <tr>
                                            <th scope="row">Postkod</th>
                                            <td id="member-postcode"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Stad</th>
                                            <td id="member-city"></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="button-field">
                                    <hr>
                                    <a id="delete_member_button" href="#" class="button"><i class="fa fa-minus-square-o"></i><span class="button-text">Ta bort</span></a>
                                    <a id="edit_member_button" class="button" href="#"><i class="fa fa-pencil-square-o"></i><span class="button-text">Ändra</span></a>
                                    <a id="create_membercard_button" class="button" target="_blank" href="#"><i class="fa fa-credit-card"></i><span class="button-text">Skapa kort</span></a>
                                </div>
                            </div>

                            <div id="grade-info" style="display: none">
                                <div class="spec-inner-wrap">
                                    <h2> Graderingar </h2>
                                    <table id="grade-table" cellpadding="2" cellspacing="1" class="display">
                                        <thead>
                                            <tr> 
                                                <th>Grad</th>
                                                <th>Datum</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="datatables_empty"><br><font color="red">Laddar grader..</font><br><br><br>
                                                   <img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'/>
                                               </td>
                                           </tr>
                                       </tbody>
                                    </table>
                                </div>
                                <div class="button-field">
                                    <hr>
                                    <a id="edit_grade_button" class="button" href="#"><i class="fa fa-pencil"></i><span class="button-text">Ändra gradering</span></a>
                                </div>
                            </div>

                            <div id="all-training_sessions" style="display: none">
                                <div class="spec-inner-wrap">
                                    <h2> Tränade pass </h2>
                                    <table id="session-table" cellpadding="2" cellspacing="1" class="display">
                                        <thead>
                                            <tr> 
                                                <th width="70%">Pass</th>
                                                <th>Datum</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="datatables_empty"><br><font color="red">Laddar grader..</font><br><br><br>
                                                   <img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'/>
                                               </td>
                                           </tr>
                                       </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="training_session-history" style="display: none">
                                <div class="spec-inner-wrap">
                                    <h2>Träningsstatistik</h2>
                                    <p>Att göra..</p>
                                    <h2>Antal träningspass</h2>
                                    <p>Räkna antalet träningspass sedan ett givet datum.</p>
                                    <p><em>Senaste graderingsdatum: <span class="grade-date"></span></em></p>
                                    <br>
                                    <input type='text' id='startdate'><a href="#" id='get-session-count'> Ok</a>
                                    <p class="session-count"><strong>Antal: </strong></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>