		<script>
		$(function() {
			$( "#form" ).validate({
				rules: {
					firstname: {
						minlength: 2,
						maxlength: 20,
						specialCharacterValidation: true
					},
					lastname: {
						minlength: 2,
						maxlength: 20,
						specialCharacterValidation: true
					},
					ssn: {
						required: false,
						minlength: 13,
						maxlength: 13,
						ssnValidation: true
					},
					email: {
						required: false,
						emailValidation: true
					},
					phone: {
						required: false,
						digits: true
					},
					postcode: {
						required: false,
						digits: true
					},
					street_address: {
						required: false,
					},
					city: {
						required: false,
					}
				},
				messages: {
					firstname: {
						required: "För att skapa medlem måste ett namn anges.",
						minlength: $.format("Förnamn måste bestå av minst två tecken."),
						maxlength: $.format("Namnet får ej vara längre än tjugo tecken.")
					},
					ssn: {
						required: "Personnummer krävs",
						minlength: $.format("Personnummer måste anges på följande sätt: yyyymmdd-xxx"),
						maxlength: $.format("Personnummer måste anges på följande sätt: yyyymmdd-xxx")
					},
					phone: {
						digits: "Ett telefonummer kan bara bestå av siffror."
					},
					postcode: {
						digits: "En postkod kan bara bestå av siffror."
					}
				}
			});
			$.validator.addMethod("specialCharacterValidation",
				function(value, element) {
					return /^[A-Öa-ö\d=#$%@_ -]+$/.test(value);
				},
				"Inga udda tecken är tillåtna."
				);
			$.validator.addMethod(
				"ssnValidation",
				function(value, element) {
					return /^\d{6}|\d{8}\-?\d{4}$/.test(value);
				},
				"Personnummers formatet skall vara: yyyymmdd-xxx"
				);
			$.validator.addMethod(
				"emailValidation",
				function(value, element) {
					return /^([0-9a-öA-Ö]([-.\w]*[0-9a-öA-Ö])*@([0-9a-öA-Ö][-\w]*[0-9a-öA-Ö]\.)+[a-öA-Ö]{2,9})$/.test(value);
				},
				"Email måste anges på formatet: example@example.com"
				);
			var date = $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" }).val();

			});
		</script>
		<div id="member_form">
			<div id="heading">
				<h2> Skapa ny medlem </h2>
			</div>
			<br>
			<form id="form" name="register" action="<?php echo site_url('/admin/new_member'); ?>" method="post" enctype="multipart/form-data">
				<table class="form-table">
					<tr>
						<th scope="row">Kortnr</th>
						<td><input type="text" name="card_nr"></td>
					</tr>
					<tr>
						<th scope="row">Namn</th>
						<td><input type="text" name="firstname"></td>
					</tr>
					<tr>
						<th scope="row">Efternamn</th>
						<td><input type="text" name="lastname"></td>
					</tr>
					<tr>
						<th scope="row">Personnummer</th>
						<td><input type="text" name="ssn"></td>
					</tr>
					<tr>
						<th scope="row">Email</th>
						<td><input type="text" name="email"></td>
					</tr>
					<tr>
						<th scope="row">Telefon</th>
						<td><input type="text" name="phone"></td>
					</tr>
					<tr>
						<th scope="row">Adress</th>
						<td><input type="text" name="street_address"></td>
					</tr>
					<tr>
						<th scope="row">Postnummer</th>
						<td><input type="text" name="postcode"></td>
					</tr>
					<tr>
						<th scope="row">Stad</th>
						<td><input type="text" name="city"></td>
					</tr>
					<tr>
						<th scope="row">Lösenord</th>
						<td><input type="password" name="password"></td>
					</tr>
					<tr>
						<th scope="row">Foto</th>
						<td><input type="file" name="photo"></td>
					</tr>
					<tr>
						<th scope="row">Instruktör</th>
						<td>
							<select name="instructor">
								<option value="0">Nej</option>
								<option value="1">Ja</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Permanent medlem</th>
						<td>
							<select name="permanent_member">
								<option value="0">Nej</option>
								<option value="1">Ja</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Administratör</th>
						<td>
							<select name="admin">
								<option value="0">Nej</option>
								<option value="1">Ja</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Avgift betalad</th>
						<td><input type="text" id="datepicker" name="payment_period"></td>
					</tr>
					<tr>
						<td>
							<a class="button" href="<?php echo site_url('/admin/members'); ?>">Avbryt</a>
						</td>
						<td>
							<input type="submit" class="button" value="Spara">
						</td>
					</tr>
				</table>
			</form>
		</div>