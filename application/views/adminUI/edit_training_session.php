<?php
	//TODO: Ajax
	//TODO: Visa instruktör
?>
		<script>
		var session_id = <?php echo $session_id; ?>;
		var rightTable;
		var leftTable;

		function initRightTable() {
			rightTable = $('#right_table').dataTable( {

				oLanguage: {
					sSearch: "Sök: ",
					sInfo: "Visar _START_ till _END_ av _TOTAL_ medlemmar...",
					sLengthMenu: "Visar _MENU_ medlemmar per sida",
					sInfoFiltered: "Filtrerat från _MAX_ totalt",
					sZeroRecords: "Inga medlemmar funna.",
					sInfoEmpty: "Inga medlemmar funna. ",
					oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
                    }
				},
				bServerSide: true,

				// Don't init the member table until the training session table has finished loading,
				// we need to know beforehand which members are already registered so we can prevent
				// double registration.
				fnInitComplete: initLeftTable,

				sDom: 'lfr<"table-inner-wrap"t>ip',
				sPaginationType: "full_numbers",
				iDisplayLength: 10,
				bLengthChange: false,
				sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_session_members/' + session_id,
				aoColumns: [
				{ mData: "firstname" },
				{ mData: "lastname" },
				{ mData: "ssn" },
				{ mData: "instructor",
					aTargets: [3],
	            	// Instuctor for this session
	                mRender: function( number ) {
	                	return number === '1' ? 'Ja' : 'Nej';
	                }
				 }],
				bJQueryUI: false, 
				aaSorting: [[0, 'asc']],
				sServerMethod: "POST"
			});

			rightTable.on('click', 'tr', function(event) {
				if ( ! rightTable.fnGetData(this) )
					return;

				$('#right_table .row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');

				var member_id = rightTable.fnGetData(this).member_id;

				$( '#middle-buttons .remove' ).attr( 'href', '<?php echo site_url("/admin/delete_member_from_training_session"); ?>/' + member_id +  '/' + session_id );

				$('.inactive-overlay').hide();
			});
		}

		function initLeftTable() {

			registeredMembers = rightTable.fnGetColumnData( 2 );

			leftTable = $('#left_table').dataTable( {

				oLanguage: {
					sSearch: "Sök: ",
					sInfo: "Visar _START_ till _END_ av _TOTAL_ medlemmar.",
					sLengthMenu: "Visar _MENU_ medlemmar per sida",
					sInfoFiltered: "Filtrerat från _MAX_ totalt",
					sZeroRecords: "Inga medlemmar funna.",
					sInfoEmpty: "Inga medlemmar funna. ",
					oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
                    }
				},
				bServerSide: true,
				fnRowCallback: function( nRow, aData ) {
					if ( registeredMembers.indexOf( aData.ssn ) >= 0 )
						$( nRow ).addClass( 'disabled' );
				},
				sDom: 'lfr<"table-inner-wrap"t>ip',
				sPaginationType: "full_numbers",
				iDisplayLength: 10,
				bLengthChange: false,
				sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_members_small',
				aoColumns: [
				{ mData: "firstname" },
				{ mData: "lastname" },
				{ mData: "ssn" }],
				bJQueryUI: false, 
				aaSorting: [[0, 'asc']], /* Sätter sortering på första kolummnen som standard */
				sServerMethod: "POST"
			});

			leftTable.on('click', 'tr', function(event) {
				if ( ! leftTable.fnGetData(this) || $(this).hasClass('disabled') )
					return;

				$('#left_table .row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');

				var member_id = leftTable.fnGetData(this).member_id;

				$( '#middle-buttons .add' ).attr( 'href', '<?php echo site_url("/admin/add_member_to_training_session"); ?>/' + member_id +  '/' + session_id + '/0' );
				$( '#middle-buttons .add-instructor' ).attr( 'href', '<?php echo site_url("/admin/add_member_to_training_session"); ?>/' + member_id +  '/' + session_id + '/1');

				$('.inactive-overlay').hide();
			});
		}

		/* DataTables */
		$(document).ready(function() {

			// This will also init the left table
			initRightTable();

		});
		</script>
		<div class="clearfix main-wrap">
			<div id="left_container" class="sixcol first">
				<div class="table-outer-wrap section-wrap">
					<div class="table-header"><h2>Alla medlemmar</h2></div>
					<table id="left_table" cellpadding="2" cellspacing="1" class="display">
						<thead>
							<tr> 
								<th>Förnamn</th>
                                <th>Efternamn</th>
                                <th>Personnummer</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3" class="datatables_empty">
									<p style="color: red">Laddar medlemmar..</p>
									<p><img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'></p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div id="middle-buttons" class="onecol">
				<div class="inactive-overlay"></div>
				<a class='button button-notext add' href='#'><i class="fa fa-arrow-right"></i></a>
				<br>
				<a class='button button-notext add-instructor' href='#'><i class="fa fa-info"></i><i class="fa fa-arrow-right"></i></a>
				<br>
				<a class='button button-notext remove' href='#'><i class="fa fa-arrow-left"></i></a>
			</div>
			<div id="right_container" class="fivecol last">
				<div class="table-outer-wrap section-wrap">
					<div class="table-header"><h2><?php echo ( $name ? $name : 'extra / ej schemalagt' ) . ' ' . $date ?></h2></div>
					<table id="right_table" cellpadding="2" cellspacing="1" class="display">
						<thead>
							<tr> 
								<th>Förnamn</th>
                                <th>Efternamn</th>
                                <th>Personnummer</th>
                                <th>Instruktör på detta pass</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4" class="datatables_empty"><br><font color="red">Laddar medlemmar från databas..</font><br><br><br>
									<img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="buttonfield">
			<a class="button" href="<?php echo site_url('/admin/training_sessions'); ?>">Tillbaka</a>
		</div>
