<?php
// TODO: Visa alla träningspass i ett table
?>
        <script>
        $(document).ready(function() {
            oTable = $('#left_table').dataTable( {

                oLanguage: {
                    sSearch: "Sök: ",
                    sInfo: "Visar _START_ till _END_ av _TOTAL_ schemapass.",
                    sLengthMenu: "Visar _MENU_ schemapass per sida",
                    sInfoFiltered: "Filtrerat från _MAX_ totalt",
                    sZeroRecords: "Inga schemapass funna.",
                    sInfoEmpty: "Inga schemapass funna. ",
                    oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
                    },
                },
                bServerSide: true,
                sDom: 'lfr<"table-inner-wrap"t>ip',
                sPaginationType: "full_numbers",
                iDisplayLength: 10,
                bLengthChange: false,
                sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_training_schedule',
                aoColumns: [
                    { mData: "name" },
                    { mData: "day",
                        aTargets: [1],
                        mRender: function( nr ) {
                            return helpers.nrToDay( nr );
                        }
                    },
                    { mData: "start_time" },
                    { mData: "end_time" }
                ],
                bJQueryUI: false, 
                aaSorting: [[0, 'asc']],
                sServerMethod: "POST"

            });

            $('#left_table').on('click', 'tr', function(event) {
                if ( ! oTable.fnGetData(this) )
                    return;

                $('#left_table .row_selected').removeClass('row_selected');
                $(this).addClass('row_selected');

                var schedule_id = oTable.fnGetData(this).schedule_id;
                var href = "<?php echo base_url(); ?>index.php/admin/get_schedule_data/" + schedule_id;

                $.ajax({
                    type : 'POST',
                    dataType: 'json',
                    url : href,
                    success : function( spec_training_session_data ) {

                        $('#spec_box h2').text( spec_training_session_data.name );
                        $("input[name='name']").val( spec_training_session_data.name );
                        $("select[name='day']").val( spec_training_session_data.day );
                        $("select[name='start_time']").val( spec_training_session_data.start_time );
                        $("select[name='end_time']").val( spec_training_session_data.end_time );
                        
                        $('#form').attr('action', '<?php echo site_url('/admin/edit_scheduled_session'); ?>/' + schedule_id );

                        $('#delete-scheduled-session').on('click', function() {
                            openModal(
                                'Ta bort träningspass?',
                                'Vill du ta bort <strong>' + spec_training_session_data.name + '</strong>? Denna åtgärd går inte att ångra!',
                                'confirm',
                                function() { window.location.href = '<?php echo site_url('/admin/delete_scheduled_session'); ?>/' + schedule_id; },
                                closeModal
                            );
                        });

                        $('#spec_box .inactive-overlay').hide()
                    }
                });
            });
        });
        </script> 
        <div id="left_container" class="sevencol first">
            <div class="table-outer-wrap section-wrap">
                <div class="table-header"><h2>Träningsschema</h2></div>
                <table id="left_table" cellpadding="2" cellspacing="1" class="display">
                    <thead>
                        <tr> 
                            <th width="40%">Namn</th>
                            <th width="20%">Dag</th>
                            <th width="15%">Starttid</th>
                            <th width="15%">Sluttid</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5" class="datatables_empty"><br><font color="red">Laddar träningsschema..</font><br><br><br>
                                <img border="0" src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="button-field">
                    <hr>
                    <a class="button" href="<?php echo site_url('/admin/new_training_session'); ?>"><i class="fa fa-plus-square-o"></i><span class="button-text">Nytt</span></a>
                </div>
            </div>
        </div>
        <div id="right_container" class="fivecol last">
            <div class="spec-wrap section-wrap">
                <div id="spec_box">
                    <div class="inactive-overlay"></div>
                        <div class="spec-outer-wrap">
                        <h2>Inget pass valt</h2>
                        <form id='form' name='register' action="#" method='post'>
                            <table class="form-table">
                                <tr>
                                    <th scope="row">Ändra namn</th>
                                    <td><input type='text' name='name' ></td>
                                </tr>
                                <tr>
                                    <th scope="row">Ändra dag</th>
                                    <td>
                                        <select name='day'>
                                            <option value='1'>Måndag</option>
                                            <option value='2'>Tisdag</option>
                                            <option value='3'>Onsdag</option>
                                            <option value='4'>Torsdag</option>
                                            <option value='5'>Fredag</option>
                                            <option value='6'>Lördag</option>
                                            <option value='7'>Söndag</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Ändra starttid</th>
                                    <td>
                                        <select name='start_time'><?php echo print_time_options(); ?></select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Ändra sluttid</th>
                                    <td>
                                        <select name='end_time'><?php echo print_time_options(); ?></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input class='button' type='submit' value='Spara'></td>
                                    <td><a class='button' id="delete-scheduled-session" href='#'>Ta bort</a></td>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>