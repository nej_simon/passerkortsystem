<?php
// TODO: Skapa pass.
?>
		<script>
		var sessionTable;
		var sessionMemberTable;
		var session_id;
		var session_name;
		var session_date;

		function initSessionTable() {
			sessionTable = $('#session-table').dataTable( {
				oLanguage: {
					sSearch: "Sök: ",
					sInfo: "Visar _START_ till _END_ av _TOTAL_ träningspass.",
					sLengthMenu: "Visar _MENU_ träningspass per sida",
					sInfoFiltered: "Filtrerat från _MAX_ totalt",
					sZeroRecords: "Inga träningspass funna.",
					sInfoEmpty: "Inga träningspass funna. ",
					oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
					},
				},
				bServerSide: true,
				sDom: 'lfr<"table-inner-wrap"t>ip',
				sPaginationType: "full_numbers",
				iDisplayLength: 10,
				bLengthChange: false,
				bSortable: true,
				sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_training_sessions',
				aoColumns: [
					{ mData: "name",
						aTargets: [0],
						mRender: function( name ) {
							return name ?name: 'Extra / ej schemalagt';
						}
					},
					{ mData: "day",
						aTargets: [1],
						mRender: function( nr ) {
							return helpers.nrToDay( nr );
						}
					},
				{ mData: "date" },
				{ mData: "start_time" },
				{ mData: "end_time" },
				{ mData: "exported",
					aTargets: [4],
					// If the session is exported
					mRender: function(type) {
						if( type === "0" ) {
							return "Nej";
						}
						return "Ja";
					}
				}],

				bJQueryUI: false,
				aaSorting: [[2, 'desc']],
				sServerMethod: "POST"
			});
		}

		function initSessionMemberTable() {
			sessionMemberTable = $('#session-member-table').dataTable( {
				oLanguage: {
					sSearch: "Sök: ",
					sInfo: "Visar _START_ till _END_ av _TOTAL_ medlemmar.",
					sLengthMenu: "Visar _MENU_ medlemmar per sida",
					sInfoFiltered: "Filtrerat från _MAX_ totalt",
					sZeroRecords: "Inga medlemmar funna.",
					sInfoEmpty: "Inga medlemmar funna. ",
					oPaginate: {
                        sFirst: "<i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>",
                        sLast: "<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>",
                        sNext: "<i class='fa fa-chevron-right'></i>",
                        sPrevious: "<i class='fa fa-chevron-left'></i>"
					},
				},
				bServerSide: false,
				sDom: '<"table-inner-wrap"t>ip',
				sPaginationType: "simple",
				iDisplayLength: 10,
				bLengthChange: false,
				bSortable: true,
				sAjaxSource: '<?php echo base_url(); ?>index.php/admin/get_session_members/' + session_id,
				aoColumnDefs: [
                    { aTargets: [2],
                    	// Print grade
                        mRender: function( number ) {
                        	return helpers.nrToKarateGrade( number );
                        }
                    },
                    { aTargets: [4],
                    	// Instuctor for this session
                        mRender: function( number ) {
                        	return number === '1' ? 'Ja' : 'Nej';
                        }
                    }],
				bJQueryUI: false,
				aaSorting: [[0, 'asc']],
				sServerMethod: "POST"
			});
		}

		/* DataTables */
		$(document).ready(function() {

			initSessionTable();

			sessionTable.on('click', 'tr', function(event) {
				if ( ! sessionTable.fnGetData(this) )
					return;

				$('#session-table .row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');

				session_id = sessionTable.fnGetData(this).session_id;
				session_name = sessionTable.fnGetData(this).name ? sessionTable.fnGetData(this).name : 'Extra / ej schemalagt';
				session_date = sessionTable.fnGetData(this).date;

				if ( ! sessionMemberTable )
					initSessionMemberTable();
				else
					sessionMemberTable.fnReloadAjax('<?php echo base_url(); ?>index.php/admin/get_session_members/' + session_id);

				$('#change-button').attr('href', '<?php echo site_url("/admin/edit_training_session"); ?>/' + session_id );
				$('#reported-button').attr('href', '<?php echo site_url("/admin/updateexported"); ?>/' + session_id );

				$('#delete-button').on('click', function() {
	                openModal(
	                    'Ta bort träningspass?',
	                    'Vill du ta bort <strong>' + session_name + ' ' + session_date + '</strong>? Denna åtgärd går inte att ångra!',
	                    'confirm',
	                    function() { window.location.href = '<?php echo site_url("/admin/delete_training_session"); ?>/' + session_id; },
	                    closeModal
	                );
            	});

                $('#spec_box .inactive-overlay').hide();
			});
		});
		</script>
		<div id="left_container" class="sevencol first">

			<div class="table-outer-wrap section-wrap">
				<div class="table-header"><h2>Träningspass</h2></div>
				<div id="table_buttons">
					<button class="button" type="button" onclick="sessionTable.fnReloadAjax('<?php echo base_url(); ?>index.php/admin/get_training_sessions');">Alla</button>
					<button class="button" type="button" onclick="sessionTable.fnReloadAjax('<?php echo base_url(); ?>index.php/admin/get_non_exported_training_sessions');">Ej rapporterade</button>
					<button class="button" type="button" onclick="sessionTable.fnReloadAjax('<?php echo base_url(); ?>index.php/admin/get_special_training_sessions');">Specialpass</button>
				</div>

				<table id="session-table" cellpadding="2" cellspacing="1" class="display">
					<thead>
						<tr>
							<th width="35%">Namn</th>
							<th width="10%">Dag</th>
							<th width="15%">Datum</th>
							<th width="10%">Starttid</th>
							<th width="10%">Sluttid</th>
							<th width="10%">Exporterad</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6" class="datatables_empty"><br><font color="red">Laddar träningspass från databas..</font><br><br><br>
								<img src='<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>'/>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div id="right_container" class="fivecol last">
			<div class="section-wrap spec-wrap">
				<div id="spec_box" class="info-box">
					<div class="inactive-overlay"></div>
					<div class="spec-outer-wrap">
                 		<div class="spec-inner-wrap">
                            <h2> Deltagare </h2>
                            <table id="session-member-table" cellpadding="2" cellspacing="1" class="display">
                                <thead>
                                    <tr>
                                        <th>Förnamn</th>
                                        <th>Efternamn</th>
                                        <th>Grad</th>
                                        <th>Personnummer</th>
                                        <th>Instruktör på detta pass</th>
                                    </tr>
                                </thead>
                                <tbody>
                               </tbody>
                            </table>
                        </div>
						<div class="button-field">
							<hr>
							<a class='button' id="change-button" href="#">Ändra</a>
							<a class='button' id="reported-button" href="#">Inrapporterat</a>
							<a class="button" id="delete-button" href="#">Radera</a>
						</div>
                    </div>
				</div>
			</div>
		</div>