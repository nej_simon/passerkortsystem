<!DOCTYPE html>
<html>
    <head>
        <title>Träningsregistrering</title>
        <meta charset='utf-8'>
        <!-- Enable strict js parsing -->
        <script>"use strict";</script>
        <script src="<?php echo base_url(); ?>assets/js/lib/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url('assets/js/modalwindows.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/util.js'); ?>"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,600,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/stylesheets/screen.css'); ?>" type="text/css" media="screen, projection"/>
        <script>

        /* global $ */
        /* global document */

        var inputEnabled = true;
        var currentInput = "";
        var inputTimeout = 750; // How long we'll wait for the input to complete, 3/4s.
        var timeout = false;

        var ajaxDef = {
            dataType: "json",
            timeout: 5000, //5 second timeout
            type: "post"
        };

        /**
         *
         * @returns {Boolean}
         */
        function reset() {

            $("#loadingpage").hide();
            timeout = false;
            currentInput = "";
            inputEnabled = true;
            return false;
        }

        /**
         * Try to authenticate a user
         * @return {boolean} false to prevent default action
         */
        function authenticateAjax() {

            $("#loadingpage").show();

            inputEnabled = false;

            var card_nr = currentInput;

            if (!card_nr) {
                $("#loadingpage").hide();
                return;
            }

            var url = '<?php echo site_url("authentication/login_user_by_card"); ?>';
            var logindata = {
                card_nr: card_nr,
                service: 'regterminal'
            };

            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                data: logindata,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', closeModal);
                    reset();
                },
                success: function(resp) {
                    if (resp.errormsg) {
                        openModal("Inloggning misslyckades!", resp.errormsg, 'cancel', closeModal);
                        reset();
                    } else {
                        window.location.replace('<?php echo site_url("regterminal") ?>');
                    }
                }
            });
            $.ajax(ajaxObj);
            return false;
        }

        /**
         * Handle input from the reader
         * @param  {[type]} e [description]
         * @return {[type]}   [description]
         */
        function handleInput( e ) {
            // if (jsDebug)
            //     saveStackTrace();

            if (inputEnabled) {

                var currentChar = String.fromCharCode( e.which );

                if ( !timeout ) {
                    $('.reading-card-loading').show();
                    timeout = window.setTimeout( function() {
                        $('.reading-card-loading').hide();
                        authenticateAjax();
                    }, inputTimeout );
                }
                if ( currentChar === '\r' ) {
                    return;
                } else {
                    currentInput += currentChar;
                }
            }
            return false;
        }

        /* Functions to run on page load */
        $(document).ready(function() {
            /* Start the clock */
            helpers.startClock();

            /* Handle input from the reader */
            $(document).keypress(handleInput);

        });
        </script>
    </head>
    <body class="authentication">
        <div id="background">
            <div id="header">
                <img src="<?php echo base_url("assets/pictures/ukklogo.svg"); ?>" id="logopicture" />
                <div id="toolbar">
                    <div id="datefield"><div id="date"></div></div>
                </div>
            </div>

            <!-- Page displaying the loading spinner -->
            <div id="loadingpage" class="fullscreen_pages">
                <img src="<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>"/>
                <br>
                <strong>Laddar...</strong>
            </div>

            <!-- Page displaying a modal dialog -->
            <div id="modalpage" class="fullscreen_pages">
                <div id="modal">
                    <div class="modalicon-field">
                        <div class="modalicon modalicon-ok"><i class="fa fa-times-circle"></i></div>
                        <div class="modalicon modalicon-cancel"><i class="fa fa-check-circle"></i></div>
                        <div class="modalicon modalicon-confirm"><i class="fa fa-question-circle"></i></div>
                    </div>
                    <div class="modalcontent">
                        <h2 class="modalcontent-header"></h2>
                        <p class="modalcontent-text"></p>
                        <p>

                            <a href="#" class="button cancel_button modalbutton-cancel modalbutton-confirm" tabindex="1">
                                <i class="fa fa-times"></i><span class="button_text">Avbryt</span>
                            </a>

                            <a href="#" class="button ok_button modalbutton-ok modalbutton-confirm" tabindex="2">
                                <i class="fa fa-check"></i><span class="button_text">Ok</span>
                            </a>

                        </p>
                    </div>

                </div>
            </div>

            <!-- Login page -->
            <div id="mainpage" class="pages">
                <div id="mainpage_container">
                    <div class="reading-card-loading">
                        <div class="reading-card-loading-wrap">
                            <img src="<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>">
                            <br>
                            <strong>Läser kort...</strong>
                        </div>
                    </div>
                    <h2>Registreringsterminalen är utloggad!</h2>
                    <p>En instruktör måste scanna sitt kort för att aktivera terminalen.</p>
                    <div class="center-picture">
                        <img src='<?php echo base_url("assets/pictures/card.svg") ?>'>
                    </div>
                </div>

            </div>
