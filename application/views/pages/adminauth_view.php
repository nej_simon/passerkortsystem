<!DOCTYPE html>
<?php
/**
 * TODO: On-screen-keyboard.
 */
?>
<html>
	<head>
		<title>Träningsregistrering</title>
		<meta charset='utf-8'>
		<!-- Enable strict js parsing -->
		<script>"use strict";</script>
		<script src="<?php echo base_url(); ?>assets/js/lib/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url('assets/js/modalwindows.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/util.js'); ?>"></script>
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,600,400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/stylesheets/screen.css'); ?>" type="text/css" media="screen, projection"/>
		<script>
			/* global $ */
			/* global window */
			/* global document */

			var ajaxDef = {
				dataType: "json",
				timeout: 5000, //5 second timeout
				type: "post"
			};

			/**
			 *
			 * @returns {Boolean}
			 */
			function reset() {

				$("#loadingpage").hide();
				$("#password").val("");

				return false;
			}

			/**
			 *
			 * @returns {undefined}
			 */
			function authUserAjax() {

				$("#loadingpage").show();

				var url = '<?php echo site_url("authentication/login_user"); ?>';
				var logindata = {
					member_id: $("#member_id").val(),
					password: $("#password").val(),
					service: 'admin'
				};

				var ajaxObj = $.extend({}, ajaxDef, {
					url: url,
					data: logindata,
					error: function(jqx, status, error) {
						openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'error', closeModal );
						reset();
					},
					success: function(resp) {
						if (resp.errormsg) {
							openModal("Inloggning misslyckades!", resp.errormsg, 'error', closeModal);
							reset();
						} else {
							switch (logindata['service']) {
								case 'regterminal':
									window.location.replace("<?php echo site_url('regterminal') ?>");
									break;
							   case 'admin':
								   window.location.replace("<?php echo site_url('admin') ?>");
								   break;
							}
						}
					}
				});
				$.ajax(ajaxObj);
				return false;
			}

			/* Functions to run on page load */
			$(document).ready(function() {
				/* Start the clock */
				helpers.startClock();

			});
		</script>
	</head>
	<body class="authentication">
		<div id="background">
			<div id="header">
				<img src="<?php echo base_url("assets/pictures/ukklogo.svg"); ?>" id="logopicture" />
				<div id="toolbar">
					<div id="datefield"><div id="date"></div></div>
				</div>
			</div>

			<!-- Page displaying the loading spinner -->
			<div id="loadingpage" class="fullscreen_pages">
				<img src="<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>"/>
				<br>
				<strong>Laddar...</strong>
			</div>

			<!-- Page displaying a modal dialog -->
			<div id="modalpage" class="fullscreen_pages">
				<div id="modal">
					<div class="modalicon-field">
						<div class="modalicon modalicon-ok"><i class="fa fa-check-circle"></i></div>
						<div class="modalicon modalicon-cancel"><i class="fa fa-times-circle"></i></div>
						<div class="modalicon modalicon-confirm"><i class="fa fa-question-circle"></i></div>
					</div>
					<div class="modalcontent">
						<h2 class="modalcontent-header"></h2>
						<p class="modalcontent-text"></p>
						<p>
							<a href="#" class="button cancel_button modalbutton-cancel modalbutton-confirm" tabindex="1">
								<i class="fa fa-times"></i><span class="button_text">Avbryt</span>
							</a>
							<a href="#" class="button ok_button modalbutton-ok modalbutton-confirm" tabindex="2">
								<i class="fa fa-check"></i><span class="button_text">Ok</span>
							</a>
						</p>
					</div>

				</div>
			</div>

			<!-- Login page -->
			<div id="mainpage" class="pages">
				<div id="mainpage_container">
					<h2>Administrationsinloggning</h2>
					<form onsubmit="return authUserAjax();">
						<table>
							<tr>
								<td>Medlemsnummer:</td>
								<td>
									<input type="text" id="member_id" autocomplete="off"/>
								</td>
							</tr>
							<tr>
								<td>Lösenord:</td>
								<td>
									<input type="password" id="password"/>
								<td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="submit" id="submit"/>
								</td>
							<tr>
						</table>
					</form>
				</div>
			</div>
