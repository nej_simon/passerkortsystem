<!DOCTYPE html>
<?php
/*
 * Mindre viktiga:
 * TODO: Make the session list scrollable if needed.
 * TODO: Localization
 *
 * Medium:
 * TODO: Manual registration.
 * TODO: Consolidate code in ajax functions
 * TODO: Fix explorer + firefox bugs.
 * TODO: Touch friendly scrollbar.
 * TODO: Save terms in the db to only show reminder after two weeks.
 * TODO: Center
 * TODO: Fix modal
 */

?>
<html>
    <head>
        <title>Träningsregistrering</title>
        <meta charset='utf-8'>
        <!-- Enable strict js parsing -->
        <script>"use strict";</script>
        <!-- Load the JS error handling before anything else -->
        <script src="<?php echo base_url('assets/js/lib/javascript-stacktrace/stacktrace.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/errorhandling.js'); ?>"></script>
        <script type="text/javascript">
        var jsDebug = <?php echo DEBUG; ?>;
        if (jsDebug) {
            window.onerror = function(message, url, linenumber) {
                var errorString = "*** SPARA DETTA FELMEDDELANDE ***\n\n";
                errorString += "JavaScript-fel: " + message + " på rad " + linenumber + " för " + url + "\n\n";
                errorString += getStackTrace();
                alert(errorString);
            }
        }
        </script>

        <script src="<?php echo base_url(); ?>assets/js/lib/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url('assets/js/lib/jquery.dataTables-1.10.2.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/modalwindows.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/util.js'); ?>"></script>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,600,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/stylesheets/screen.css'); ?>" type="text/css" media="screen, projection">

        <script>
        /* global $ */
        /* global window */
        /* global document */
        /* global console */

        var regdata = {};
        var userdata = {};
        var allSessions = {};
        var registeredSessions = {};
        var defaultUserPhoto = '<?php echo base_url("assets/pictures/userphoto_default.jpg"); ?>';

        var currentInput = "";
        var inputTimeout = 750; // How long we'll wait for the input to complete, 3/4s.
        var inputEnabled = true;
        var timeout = false;

        /* Default ajax settings */
        var ajaxDef = {
            dataType: "json",
            timeout: 5000 //5 second timeout
        };

        /* templates (sort of) */
        var page2SessionInfoDef;

        /* timecounter variables */
        var hourStep = 1;
        var minuteStep = 5;
        var hoursStart;
        var minutesStart;
        var hoursEnd;
        var minutesEnd;

        function initTimeCounter() {
            if (jsDebug)
                saveStackTrace();

            var now = new Date();
            var hour = helpers.prependZero(now.getHours());
            var minute = helpers.prependZero(now.getMinutes());

            var startHour = (hour - hour % hourStep) % 24;
            var startMinute = (minute - minute % minuteStep) % 60;

            var endHour = (startHour + hourStep) % 24;
            var endMinute = startMinute % 60;

            $("#timefield_hours_start").text(startHour);
            $("#timefield_minutes_start").text(startMinute);
            $("#timefield_hours_end").text(endHour);
            $("#timefield_minutes_end").text(endMinute);
        }

        /**
         * Select one of the training session boxes.
         * @returns {Boolean} false to prevent the browser from opening the href
         */
        function selectSessionBox() {
            if (jsDebug)
                saveStackTrace();

            var currentSelectedBox = $(".trainingsession_box_selected");
            var infoPages = $(".sessioninfo_page");
            var clickedBox = $(this);

            if (currentSelectedBox.length === 0) {
                $("#sessioninfo_greetingheader").hide(); //Hide the welcome message
            } else {
                currentSelectedBox.removeClass("trainingsession_box_selected");
                infoPages.hide();

            }

            if (clickedBox.attr('id') === "newssesion_box") {

                initTimeCounter();
                $("#sessioninfo_newsession").show();

            } else {

                var session_id = clickedBox.data('session_id');
                $("#sessioninfo_session_" + session_id).show();
                regdata.session_id = session_id;

            }

            clickedBox.addClass("trainingsession_box_selected");
            return false;

        }

        function toggleTimeCounter() {
            if (jsDebug)
                saveStackTrace();

            var clickedButton = $(this);

            switch (clickedButton.attr("id")) {
                case ("start_hour_up"):
                    hoursStart.text(helpers.prependZero((parseInt(hoursStart.text(), 10) + hourStep) % 24));
                    break;
                case ("start_minute_up"):
                    minutesStart.text(helpers.prependZero((parseInt(minutesStart.text(), 10) + minuteStep) % 60));
                    break;
                case ("start_hour_down"):
                    hoursStart.text(helpers.prependZero((parseInt(hoursStart.text(), 10) - hourStep + 24) % 24));
                    break;
                case ("start_minute_down"):
                    minutesStart.text(helpers.prependZero((parseInt(minutesStart.text(), 10) - minuteStep + 60) % 60));
                    break;
                case ("end_hour_up"):
                    hoursEnd.text(helpers.prependZero((parseInt(hoursEnd.text(), 10) + hourStep) % 24));
                    break;
                case ("end_minute_up"):
                    minutesEnd.text(helpers.prependZero((parseInt(minutesEnd.text(), 10) + minuteStep) % 60));
                    break;
                case ("end_hour_down"):
                    hoursEnd.text(helpers.prependZero((parseInt(hoursEnd.text(), 10) - hourStep + 24) % 24));
                    break;
                case ("end_minute_down"):
                    minutesEnd.text(helpers.prependZero((parseInt(minutesEnd.text(), 10) - minuteStep + 60) % 60));
                    break;
                default:
                    console.log("toggleTimeCounter: unknown source " + this);
                    break;
            }
            return false;
        }

        /**
         * Find overlaping training sessions
         * @param {array} registeredSession Registered training sessions
         * @param {array} allSessions All training sessions
         * @returns {array} Overlaping sessions, if any
         */
        function findSessionOverlap(registeredSession, allSessions) {
            if (jsDebug)
                saveStackTrace();

            var overlappingSessions = {};
            var startTimeUnix = helpers.parseDateTime(registeredSession.date, registeredSession.start_time);
            var endTimeUnix = helpers.parseDateTime(registeredSession.date, registeredSession.end_time);

            for (var i = 0; i < allSessions.length; i++) {

                if (allSessions[i] === registeredSession) {
                    continue;
                }

                var currentStartTimeUnix = helpers.parseDateTime(allSessions[i].date, allSessions[i].start_time);
                var currentEndTimeUnix = helpers.parseDateTime(allSessions[i].date, allSessions[i].end_time);

                if (!((startTimeUnix <= currentStartTimeUnix && endTimeUnix <= currentStartTimeUnix) || startTimeUnix >= currentEndTimeUnix)) {
                    overlappingSessions[allSessions[i].session_id] = allSessions[i];
                }
            }

            return overlappingSessions;
        }

        /**
         * Reset the registration terminal to the initial state
         * @returns {Boolean}
         */
        function reset() {
            if (jsDebug)
                saveStackTrace();

            regdata = {};
            userdata = {};
            allSessions = {};
            registeredSessions = {};
            inputEnabled = true;
            currentInput = "";
            timeout = false;

            $('#title').html('Träningsregistrering');

            $('#userbox').hide();
            $(".trainingsession_box_temp").remove();
            $(".sessioninfo_temp").remove();
            $("#sessioninfo_greetingheader").show();
            $(".trainingsession_box").off("click");
            $(".trainingsession_box_selected").removeClass("trainingsession_box_selected");
            $("#sessioninfo_newsession").hide();
            $('.toolbar_button').hide();
            $('.instructoroptions').hide();

            $('#page2').hide();
            $('#page1').show();

            $("#loadingpage").hide();

            return false;
        }

        /**
         * Create the box with user info
         * @param {array} userData Data about the user
         * @returns {undefined} Nothing lol
         */
        function createUserBox(userData) {
            if (jsDebug)
                saveStackTrace();

            $("#userbox_name").html(userData.firstname + '<br>' + userData.lastname);
            $("#userbox_kind").html(
                    (userData.instructor === '1' ? 'Instruktör' : 'Medlem') + " &bull; " + helpers.nrToKarateGrade(userData.grade));

            if (userData.photo === null) {
                $("#userbox_photo").attr("src", defaultUserPhoto);
            } else {
                $("#userbox_photo").attr("src", userData.photo);
            }
        }

        /**
         * Append the DOM with training session info and buttons to select them
         * @param {array} allSessions All sessions
         * @param {array} registeredSessions Registered sessions
         * @param {array} overlappingSessions Overlapping sessions
         * @returns {undefined} nothing lol
         */
        function createTrainingSessions(allSessions, registeredSessions, overlappingSessions) {
            if (jsDebug)
                saveStackTrace();

            newSessionBox = $("#newssesion_box").detach();

            //TODO: Replace with a detachable element
            for (var i = 0; i < allSessions.length; i++) {

                var sessionTitle = allSessions[i].name ? allSessions[i].name : "Extra: " + (allSessions[i].instructors === null ? "Ingen instruktör" : allSessions[i].instructors);

                /* Create the button */

                var box = $("<a></a>").addClass("trainingsession_box trainingsession_box_temp")
                        .data("session_id", allSessions[i].session_id)
                        .attr("href", "#");
                var nameSpan = $("<span></span>")
                        .addClass("trainingsession_name")
                        .append(sessionTitle);
                var timeSpan = $("<span></span>")
                        .addClass("trainingsession_time")
                        .append(allSessions[i].start_time + " - " + allSessions[i].end_time);
                box.append(nameSpan).append("<br>").append(timeSpan);

                /* Create the session info page */

                // Clone the template and add id
                var info = page2SessionInfoDef.clone(true)
                        .attr("id", "sessioninfo_session_" + allSessions[i].session_id);

                // Set the title
                info.find(".sessioninfo_header h2").text(sessionTitle);

                info.find(".sessioninfo_header ul")
                        .append("<li>Tränare: " + (allSessions[i].instructors ? allSessions[i].instructors : "Ingen registrerad") + "</li>")
                        .append("<li>Antal registrerade: " + allSessions[i].nr_registered + "</li>");

                var colorDiv = $("<div></div>");

                // Add apropriate buttons depending on the session
                if (registeredSessions[allSessions[i].session_id]) {
                    info.find(".unregisterbutton").show();
                    colorDiv.addClass("registration_indicator_blue");
                } else if (overlappingSessions[allSessions[i].session_id]) {
                    info.find(".sessioninfo_footer").append("<em>Du kan ej registrera dig på detta träningspass då du redan är registrerad på ett överlappande!</em>");
                    colorDiv.addClass("registration_indicator_red");
                } else {
                    info.find(".registerbutton").show();
                    if (userdata.instructor === '1') {
                        info.find(".instructor_registerbutton").show();
                    }
                    colorDiv.addClass("registration_indicator_green");
                }

                // Create the datatable
                var infoTable = info.find(".participant_table");
                infoTable.attr("id", "participant_table_session_" + allSessions[i].session_id);

                var dataTable = infoTable.dataTable({
                    //sPaginationType: "simple",
                    //iDisplayLength: 10,
                    bLengthChange: false,
                    bInfo: false,
                    bPaginate: false,
                    sAjaxSource: '<?php echo site_url("regterminal/get_session_members_table") ?>' + '/' + allSessions[i].session_id,
                    aoColumnDefs: [{
                            aTargets: [2],
                            mRender: function(data) {
                                return helpers.nrToKarateGrade(data);
                            }
                        },
                        {
                            aTargets: [3],
                            mRender: function( data, type, row ) {
                                var date = row[3];
                                var instructor = row[4];
                                var perm_member = row[5];
                                if ( instructor === '0' && perm_member === '0' ) {
                                    var errorString = '<span class="error">Obetald avgift!</span>';

                                    if ( data === null ) {
                                        return errorString;
                                    }

                                    if ( ! helpers.checkPaymentStatus( date ) ) {
                                        return errorString;
                                    }

                                    return data;
                                }
                                else
                                    return "";
                                }
                        },
                        {
                            bVisible: false,
                            aTargets: [ 4, 5 ]
                        }],
                    bJQueryUI: false,
                    bFilter: false,
                    aaSorting: [[0, 'asc']]
                });

                $("#sessionlist").append(box.append(colorDiv));
                $("#sessioninfo").append(info);
            }

            $("#sessionlist").append(newSessionBox);
            $(".trainingsession_box").on("click", selectSessionBox);

        }

        /**
         *
         * @returns {Boolean} always false to prevent the default action
         */
        function initRegistrationAjax() {
            if (jsDebug)
                saveStackTrace();

            inputEnabled = false;

            $("#loadingpage").show();

            var card_nr = currentInput;

            if (!card_nr) {
                $("#loadingpage").hide();
                reset();
                return;
            }

            var now = helpers.getDateTimeObj();
            var url = '<?php echo site_url("regterminal/init_registration"); ?>' + '/' + card_nr + '/' + now.date + '/' + now.time;
            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', function() {
                        closeModal();
                        reset();
                    });
                },
                success: function(resp) {
                    if (resp.errormsg) {

                        openModal("Registrering misslyckades!", resp.errormsg, 'cancel',
                                function() {
                                    closeModal();
                                    if (resp.logout) {
                                        window.location.replace("<?php echo site_url() ?>");
                                    }
                                    reset();
                                });

                    } /*else if (resp.training_sessions.length === 0 && resp.user_data.instructor === '0') {

                        openModal("Inga pass!", "Just nu finns det inga träningspass att registrera sig på!", 'ok', function() {
                            closeModal();
                            inputEnabled = true;
                            $("#loadingpage").hide();
                        });

                    } */ else {

                        regdata.member_id = resp.user_data.member_id;
                        userdata = resp.user_data;

                        var overlappingSessions = {};
                        allSessions = resp.training_sessions;
                        var alreadyRegistered = false;

                        for (var i = 0; i < allSessions.length; i++) {

                            if (allSessions[i].registered === '1') {
                                registeredSessions[allSessions[i].session_id] = allSessions[i];
                                $.extend(overlappingSessions, findSessionOverlap(allSessions[i], allSessions));
                                alreadyRegistered = true;
                            }
                        }

                        createTrainingSessions(allSessions, registeredSessions, overlappingSessions);
                        createUserBox(resp.user_data);

                        $('#title').html('Välj träningspass');
                        $('#userbox').show();
                        $('.toolbar_button').css('display', 'inline-block'); // .show() would set the display-property to 'inline'
                        if (resp.user_data.instructor === '1') {
                            $('.instructoroptions').show();
                        } else {
                            $('.instructoroptions').hide();
                        }
                        $('#page1').hide();
                        $('#page2').show();
                        if (alreadyRegistered) {
                            openModal("Redan registrerad!", "<p>Du är redan registrerad på ett träningspass. Du kan:<br><ul><li>Registrera dig på ett annat, ej överlappande, träningspass.</li><li>Avregistrera dig.</li></ul><em>För att ändra en registrering, avregistrera dig från det andra träningspasset först!</em></p>", 'ok', closeModal);
                        }

                        if ( resp.user_data.permanent_member === '0' && resp.user_data.instructor === '0' && ! helpers.checkPaymentStatus( resp.user_data.payment_period ) ) {
                            openModal("Obetald avgift!", "<p>Din avgift för den här terminen är ej betald!</p><p><em> Om du nyligen har betalat eller tränar inom prövotiden kan du bortse från det här meddelandet.</em></p>", 'cancel', closeModal);
                        }
                        $("#loadingpage").hide();
                    }
                }
            });
            $.ajax(ajaxObj);
            return false;
        }

        /**
         *
         * @returns {undefined}
         */
        function registerAjax(data) {
            if (jsDebug)
                saveStackTrace();

            $("#loadingpage").show();

            if (data.data && userdata.instructor === '1') {
                regdata.instructor = data.data.instructor;
            } else {
                regdata.instructor = '0';
            }

            var now = helpers.getDateTimeObj();
            var url = '<?php echo site_url("regterminal/register_user"); ?>' + '/' + regdata.member_id + '/' + regdata.instructor + '/' + regdata.session_id + '/' + now.time;

            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', closeModal);
                    reset();
                },
                success: function(resp) {
                    if (resp.errormsg) {
                        openModal("Registrering misslyckades!", resp.errormsg, 'cancel',
                                function() {
                                    closeModal();
                                    if (resp.logout) {
                                        window.location.replace("<?php echo site_url() ?>");
                                    }
                                });
                    }
                    reset();
                }
            });
            $.ajax(ajaxObj);
            return false;
        }

        /**
         *
         * @returns {undefined}
         */
        function createSessionAjax() {
            if (jsDebug)
                saveStackTrace();

            var startTime = hoursStart.text() + ":" + minutesStart.text() + ":00";
            var endTime = hoursEnd.text() + ":" + minutesEnd.text() + ":00";
            var now = helpers.getDateTimeObj();

            overlappingSessions = findSessionOverlap({
                'session_id': 0,
                'start_time': startTime,
                'end_time': endTime,
                'date': now.date
            }, $.map(registeredSessions, function (value, key) { return value; }));

            if (!$.isEmptyObject(overlappingSessions)) {
                openModal("Skapa pass misslyckades!", "Du är registrerad på ett överlappande pass! Avregistrera dig från det innan, eller välj en annan tid!", 'cancel', closeModal);
                return;
            }

            if (helpers.parseDateTime(now.date, startTime) >= helpers.parseDateTime(now.date, endTime)) {
                openModal("Skapa pass misslyckades!", "Sluttiden måste vara efter starttiden!", 'cancel', closeModal);
                return;
            }

            $("#loadingpage").show();

            var url = '<?php echo site_url("regterminal/create_session"); ?>' + '/' + userdata.member_id + '/' + now.date + '/' + startTime + '/' + endTime;

            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', closeModal);
                    reset();
                },
                success: function(resp) {
                    if (resp.errormsg) {
                        openModal("Skapa pass misslyckades!", resp.errormsg, 'cancel',
                                function() {
                                    closeModal();
                                    if (resp.logout) {
                                        window.location.replace("<?php echo site_url() ?>");
                                    }
                                });
                    }
                    reset();
                }
            });
            $.ajax(ajaxObj);
            return false;
        }

        /**
         *
         * @returns {undefined}
         */
        function unregisterAjax() {
            if (jsDebug)
                saveStackTrace();

            $("#loadingpage").show();

            var url = '<?php echo site_url("regterminal/deregister_user"); ?>' + '/' + regdata.member_id + '/' + regdata.session_id;

            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', closeModal);
                    reset();
                },
                success: function(resp) {
                    if (resp.errormsg) {
                        openModal("Registrering misslyckades!", resp.errormsg, 'cancel',
                                function() {
                                    closeModal();
                                    if (resp.logout) {
                                        window.location.replace("<?php echo site_url() ?>");
                                    }
                                    reset();
                                });
                    } else {
                        reset();
                    }

                }
            });
            $.ajax(ajaxObj);
            return false;

        }

        /**
         *
         * @returns {undefined}
         */
        function endRegistrationAjax() {
            if (jsDebug)
                saveStackTrace();

            $("#loadingpage").show();

            var url = '<?php echo site_url("regterminal/end_registration"); ?>';

            var ajaxObj = $.extend({}, ajaxDef, {
                url: url,
                error: function(jqx, status, error) {
                    openModal("Serverfel!", "Typ: " + status + "<br>Meddelande: " + error, 'cancel', closeModal);
                    reset();
                },
                success: function(resp) {
                    reset();
                }
            });
            $.ajax(ajaxObj);
            return false;

        }

        /**
         * Handle input from the reader
         * @param  {[type]} e [description]
         * @return {[type]}   [description]
         */
        function handleInput( e ) {
            if (jsDebug)
                saveStackTrace();

            if (inputEnabled) {

                var currentChar = String.fromCharCode( e.which );

                if ( !timeout ) {
                    $('.reading-card-loading').show();
                    timeout = window.setTimeout( function() {
                        $('.reading-card-loading').hide();
                        initRegistrationAjax();
                    }, inputTimeout );
                }
                if ( currentChar === '\r' ) {
                    return;
                } else {
                    currentInput += currentChar;
                }
            }
            return false;
        }

        /* Functions to run on page load */
        $(document).ready(function() {

            /* Start the clock */
            helpers.startClock();

            /* Attach event handlers */
            $(".plusbuttons, .minusbuttons").on("click", toggleTimeCounter);
            $(".registerbutton .button").on("click", registerAjax);
            $(".instructor_registerbutton .button").on("click", {instructor: 1}, registerAjax);
            $(".unregisterbutton .button").on("click", unregisterAjax);
            $("#sessioninfo_newsession .button").on("click", createSessionAjax);
            $("#toolbar_button_back").on("click", endRegistrationAjax);

            /* Some jquery objects that will be used later by the timecounter */
            hoursStart = $("#timefield_hours_start");
            minutesStart = $("#timefield_minutes_start");
            hoursEnd = $("#timefield_hours_end");
            minutesEnd = $("#timefield_minutes_end");

            page2SessionInfoDef = $("#sessioninfo_page_template").detach();

            $(document).keypress(handleInput);
        });

        </script>

    </head>
    <body class="regterminal">

        <div id="sessioninfo_page_template" class='sessioninfo_page sessioninfo_temp'>
            <div class="sessioninfo_header">
                <h2></h2>
                <hr>
                <strong><ul></ul></strong>
            </div>
            <div class="sessioninfo_content">
                <table class='participant_table' border='1' cellpadding='2' cellspacing='1' class='display'>
                    <thead>
                        <tr>
                            <th>Förnamn</th>
                            <th>Efternamn</th>
                            <th>Grad</th>
                            <th>Medlemskap</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='4' class='datatables_empty'>
                                <font color='red'>Välj ett pass för att se dess medlemmar</font>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="sessioninfo_footer">
                <span class='registerbutton'>
                    <a href='#' class='button ok_button'>
                        <span class="button_text">Registrera</span><i class="fa fa-check"></i>

                    </a>
                </span>
                <span class='instructor_registerbutton'>
                    <a href='#' class='button ok_button'>
                        <span class="button_text">Registrera (instruktör)</span><i class="fa fa-check"></i>
                    </a>
                </span>
                <span class='unregisterbutton'>
                    <a href='#' class='button cancel_button'>
                        <span class="button_text">Avregistrera</span><i class="fa fa-times"></i>
                    </a>
                </span>
            </div>
        </div>


        <div id="background">
            <div id="header">
                <img src="<?php echo base_url("assets/pictures/ukklogo.svg"); ?>" id="logopicture">

                <div id="toolbar">
                    <div id="datefield"><div id="date"></div></div>
                    <div id="userbox">
                        <img src="" id="userbox_photo">
                        <div id="userbox_info">
                            <span id="userbox_name"></span>
                            <br>
                            <span id="userbox_kind"></span>
                        </div>
                    </div>

                    <!-- Toolbar buttons -->
                    <!--<a href="#" class="toolbar_button instructoroptions" id="toolbar_button_options">
                        <img src="<?php echo base_url('assets/pictures/icons/options.svg'); ?>">
                        <br>
                        <strong>Alternativ</strong>
                    </a> -->
                    <a href="#" class="toolbar_button toolbar_button_selected" id="toolbar_button_registration">
                        <img src="<?php echo base_url('assets/pictures/icons/oklarge.svg'); ?>">
                        <br>
                        <strong>Registrera träning</strong>
                    </a>
                    <a href="#" class="toolbar_button" id="toolbar_button_back">
                        <img src="<?php echo base_url('assets/pictures/icons/backarrow.svg'); ?>">
                        <br>
                        <strong>Tillbaka</strong>
                    </a>
                </div>
            </div>

            <!-- Page displaying the loading spinner -->
            <div id="loadingpage" class="fullscreen_pages">
                <img src="<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>">
                <br>
                <strong>Laddar...</strong>
            </div>

            <!-- Page displaying a modal dialog -->
            <div id="modalpage" class="fullscreen_pages">
                <div id="modal">
                    <div class="modalicon-field">
                        <div class="modalicon modalicon-ok"><i class="fa fa-check-circle"></i></div>
                        <div class="modalicon modalicon-cancel"><i class="fa fa-times-circle"></i></div>
                        <div class="modalicon modalicon-confirm"><i class="fa fa-question-circle"></i></div>
                    </div>
                    <div class="modalcontent">
                        <h2 class="modalcontent-header"></h2>
                        <p class="modalcontent-text"></p>
                        <p>

                            <a href="#" class="button cancel_button modalbutton-cancel modalbutton-confirm" tabindex="1">
                                <i class="fa fa-times"></i><span class="button_text">Ok</span>
                            </a>

                            <a href="#" class="button ok_button modalbutton-ok modalbutton-confirm" tabindex="2">
                                <i class="fa fa-check"></i><span class="button_text">Ok</span>
                            </a>

                        </p>
                    </div>

                </div>
            </div>

            <!-- First registration page -->
            <div id="page1" class="pages">
                <div id="page1_container">
                    <div class="reading-card-loading">
                        <div class="reading-card-loading-wrap">
                            <img src="<?php echo base_url("assets/pictures/icons/loading2.gif"); ?>">
                            <br>
                            <strong>Läser kort...</strong>
                        </div>
                    </div>
                    <h2>Var god läs in ditt medlemskort!</h2>
                    <img class="greeting-picture" src='<?php echo base_url("assets/pictures/card.svg") ?>'>
                </div>
            </div>

            <!-- Second registration page -->
            <div id="page2" class="pages">
                <div id="page2_container">
                    <div id="sessionlist">
                        <a href="#" class="trainingsession_box" id="newssesion_box">
                            <span class="trainingsession_name">Nytt träningspass</span>
                        </a>
                    </div>
                    <div id="sessioninfo">
                        <h2 id="sessioninfo_greetingheader">Välj önskat träningspass!</h2>
                        <div id="sessioninfo_newsession" class="sessioninfo_page">
                            <div class="newsession_header">
                                <h2>Nytt träningspass</h2>
                                <hr>
                            </div>
                            <div class="newsession_content">
                                <div class="timecounter_container">
                                    <h3>Starttid</h3>
                                    <div class="timecounter" id="starttime">
                                        <div class="hour">
                                            <a href="#" class="plusbuttons" id="start_hour_up">
                                                <img src="<?php echo base_url("assets/pictures/icons/plus.svg") ?>" class="buttonicon">
                                            </a>
                                            <a href="#" class="minusbuttons" id="start_hour_down">
                                                <img src="<?php echo base_url("assets/pictures/icons/minus.svg") ?>" class="buttonicon">
                                            </a>
                                        </div>
                                        <div class="minute">
                                            <a href="#" class="plusbuttons" id="start_minute_up">
                                                <img src="<?php echo base_url("assets/pictures/icons/plus.svg") ?>" class="buttonicon">
                                            </a>
                                            <a href="#" class="minusbuttons" id="start_minute_down">
                                                <img src="<?php echo base_url("assets/pictures/icons/minus.svg") ?>" class="buttonicon">
                                            </a>
                                        </div>
                                        <div class="timefield">
                                            <span id="timefield_hours_start"></span>
                                            <span id="timefield_minutes_start"></span></div>
                                    </div>
                                </div>
                                <div class="timecounter_container">
                                    <h3>Sluttid</h3>
                                    <div class="timecounter" id="endtime">
                                        <div class="hour">
                                            <a href="#" class="plusbuttons" id="end_hour_up">
                                                <img src="<?php echo base_url("assets/pictures/icons/plus.svg") ?>" class="buttonicon">
                                            </a>
                                            <a href="#" class="minusbuttons" id="end_hour_down">
                                                <img src="<?php echo base_url("assets/pictures/icons/minus.svg") ?>" class="buttonicon">
                                            </a>
                                        </div>
                                        <div class="minute">
                                            <a href="#" class="plusbuttons" id="end_minute_up">
                                                <img src="<?php echo base_url("assets/pictures/icons/plus.svg") ?>" class="buttonicon">
                                            </a>
                                            <a href="#" class="minusbuttons" id="end_minute_down">
                                                <img src="<?php echo base_url("assets/pictures/icons/minus.svg") ?>" class="buttonicon">
                                            </a>
                                        </div>
                                        <div class="timefield">
                                            <span id="timefield_hours_end"></span>
                                            <span id="timefield_minutes_end"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="sessioninfo_footer">
                                <a href='#' class='button ok_button'><span class="button_text">Skapa</span><i class="fa fa-check"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
