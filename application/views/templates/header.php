<!DOCTYPE html>
<html>
	<head>
		<title><?php echo ( $title ? $title : '' ) ?></title>
		<meta charset="utf-8">
		<!-- Enable strict js parsing -->
		<script>"use strict";</script>
		<script src="<?php echo base_url(); ?>assets/js/lib/jquery-2.1.1.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/lib/jquery-ui-1.11.1.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/lib/jquery.validate-1.13.0.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/lib/slimbox2.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/lib/jquery.dataTables-1.10.2.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/lib/dataTables-plugins.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/util.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/modalwindows.js"></script>
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheets/screen.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin_style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-smoothness.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slimbox2.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,600,400,700' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<header class="header">
			<div class="inner-header">
				<a class="button" href="<?php echo site_url(); ?>/authentication/logout" style="float: right;"><span class="button-text">Logga ut!</span><i class="fa fa-sign-out"></i></a>
				<div class="logo">
					<img src="<?php echo base_url(); ?>/assets/pictures/ukklogo-small.svg" id="header_logopicture">
				</div>
				<div class="title"><h1>Administration</h1></div>
			</div>
			<nav id="menu"><?php echo $menu ?></nav>
		</header>

        <!-- Page displaying a modal dialog -->
        <div id="modalpage" class="fullscreen_pages">
            <div id="modal">
                <div class="modalicon-field">
                    <div class="modalicon modalicon-ok"><i class="fa fa-check-circle"></i></div>
                    <div class="modalicon modalicon-cancel"><i class="fa fa-times-circle"></i></div>
                    <div class="modalicon modalicon-confirm"><i class="fa fa-question-circle"></i></div>
                </div>
                <div class="modalcontent">
                    <h2 class="modalcontent-header"></h2>
                    <p class="modalcontent-text"></p>
                    <p>
                    	<a href="#" class="button cancel_button modalbutton-cancel modalbutton-confirm" tabindex="1">
                            <i class="fa fa-times"></i><span class="button_text">Avbryt</span>
                        </a>

                        <a href="#" class="button ok_button modalbutton-ok modalbutton-confirm" tabindex="2">
                            <i class="fa fa-check"></i><span class="button_text">Ok</span>
                        </a>

                    </p>
                </div>

            </div>
        </div>

		<div id="main" class="clearfix">