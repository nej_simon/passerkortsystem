var modalQueue = [];

/**
 * 
 * @param {type} text
 * @param {type} title
 * @param {type} callback
 * @param {type} type
 * @returns {undefined}
 */
function openModal( title, text, type, callback_1, callback_2 ) {

    if (modalQueue.length === 0) {

        modalQueue.unshift(arguments);

        $("#modal .modalcontent-text").html(text);
        $("#modal .modalcontent-header").html(title);
        $("#modal .button").hide();
        $("#modal .modalicon").hide();

        switch (type) {
            case "ok":
                $("#modal .modalbutton-ok").on("click", callback_1).show();
                $("#modal .modalicon-ok").show();
                break;
            case "cancel":
            case "error":
                $("#modal .modalbutton-cancel").on("click", callback_1).show();
                $("#modal .modalicon-cancel").show();
                break;
            case "confirm":
                $("#modal .modalbutton-ok").on("click", callback_1).show();
                $("#modal .modalbutton-cancel").on("click", callback_2).show();
                $("#modal .modalicon-confirm").show();
                break;
            default:
                console.log("Open modal failed: invalid arguments.");
                break;
        }

        $("#modalpage").show();
        if ( type === 'ok' )
            $("#modal .modalbutton-ok").focus();
        else
            $("#modal .modalbutton-cancel").focus();
    } else {
        modalQueue.unshift(arguments);
    }
}

/**
 *
 * @returns {undefined}
 */
function closeModal() {

    $("#modal .button").off('click');
    $("#modalpage").hide();
    
    modalQueue.pop();

    if (modalQueue.length !== 0) {
        nextModal = modalQueue.pop();
        openModal(nextModal[0], nextModal[1], nextModal[2], nextModal[3], nextModal[4]);
    }
}