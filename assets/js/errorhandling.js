var trace = "";

function saveStackTrace() {
    trace = printStackTrace().join('\n\n');
}

function getStackTrace() {
    return trace;
}