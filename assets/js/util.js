"use strict";

/* global $ */
/* global document */
/* global window */

var helpers = {
    /**
     * [prependZero description]
     * @param  {[type]} number [description]
     * @return {[type]}        [description]
     */
    prependZero: function (number) {
        return number < 10 ? '0' + number : number;
    },

    /**
     *
     * @returns {unresolved}
     */
    getDateTimeObj: function () {
        var now = new Date();

        var hour = this.prependZero(now.getHours());
        var minute = this.prependZero(now.getMinutes());
        var second = this.prependZero(now.getSeconds());
        var day = this.prependZero(now.getDate());
        var month = this.prependZero(now.getMonth() + 1); // Months also have a zero index
        var year = now.getFullYear();

        return {
            date: (year + '-' + month + '-' + day),
            time: (hour + ":" + minute + ":" + second)
        };
    },

    /**
     * Clock script
     */
    startClock: function ()
    {
        var now = this.getDateTimeObj();
        $('#header #date').html(now.date + '<br />' + now.time);
        setTimeout(function() {
            helpers.startClock();
        }, 500);
    },

    /**
     * Very simple replacement for javascript's Date.parse() which doesn't work reliably across browsers
     * @param {string} date a date string of the format 2013-05-01
     * @param {string} time a date string of the format 07:30:10 (optional)
     * @returns {int} Unix time or whatever javascript uses
     */
    parseDateTime: function ( date, time ) {
        var dateComponents = date.split("-");

        if ( time ) {
            var timeComponents = time.split(":");
            return new Date(dateComponents[0], dateComponents[1] - 1,
                    dateComponents[2], timeComponents[0],
                    timeComponents[1], timeComponents[2]).getTime();
        } else {
            return new Date( dateComponents[0], dateComponents[1] - 1 ).getTime();
        }

    },

    /**
     * [checkPaymentStatus description]
     * @param  {[type]} userData [description]
     * @return {[type]}          [description]
     */
    checkPaymentStatus: function ( date ) {

        if ( ! date ) {
            return false;
        }

        var paymentTime = this.parseDateTime( date );
        var now = new Date().getTime();

        if ( now > paymentTime ) {
            return false;
        } else {
            return true;
        }
    },

    /* Barnbälten
     * 12 Mon (Vitt bälte med ett streck)
     * 11 Mon (Vitt bälte med två streck)
     * 10 Mon (Orange bälte)
     * 9 Mon (Orange bälte med streck)
     * 8 Mon (Blått bälte)
     * 7 Mon (Blått bälte med streck)
     * 6 Mon (Gult bälte)
     * 5 Mon (Gult bälte med streck)
     * 4 Mon (Grönt bälte)
     * 3 Mon (Grönt bälte med streck)
     * 2 Mon (Brunt bälte)
     * 1 Mon (Brunt bälte med streck)
     *
     * Vuxenbälten
     * 10 Kyu (Orange bälte)
     * 9 Kyu (Orange bälte med streck)
     * 8 Kyu (Blått bälte)
     * 7 Kyu (Blått bälte med streck)
     * 6 Kyu (Gult bälte)
     * 5 Kyu (Gult bälte med streck)
     * 4 Kyu (Grönt bälte)
     * 3 Kyu (Grönt bälte med streck)
     * 2 Kyu (Brunt bälte)
     * 1 Kyu (Brunt bälte med streck)
     * 1 Dan (Svart bälte med 1 streck)
     * 2 Dan (Svart bälte med 2 streck)
     * 3 Dan (Svart bälte med 3 streck)
     * 4 Dan (Svart bälte med 4 streck)
     * 5 Dan (Svart bälte med 5 streck)
     * 6 Dan (Svart bälte med 6 streck)
     * 7 Dan (Svart bälte med 7 streck)
     * 8 Dan (Svart bälte med 8 streck)
     * 9 Dan (Svart bälte med 9 streck)
     * 10 Dan (Svart bälte med 10 streck) */

    nrToKarateGrade: function (number) {
        if ( ! number || number < 1 || number > 34 )
            return "";
        else if(number >= 1 && number <= 12)
            return 13 - number + " Mon";
        else if(number >= 13 && number <= 22)
            return 23 - number + " Kyu";
        else if(number >= 23 && number <= 32)
            return number - 22 + " Dan";
    },

    nrToDay: function ( nr ) {
        switch ( nr ) {
            case "1":
                return "Måndag";
            case "2":
                return "Tisdag";
            case "3":
                return "Onsdag";
            case "4":
                return "Torsdag";
            case "5":
                return "Fredag";
            case "6":
                return "Lördag";
            case "7":
                return "Söndag";
            default:
                return '';
        }
    },

    /**
     * Creates a URL from a base URL and a an array of arguments
     * @param  {[type]} base [description]
     * @param  {[type]} args [description]
     * @return {[type]}      [description]
     */
    createURL: function ( base, args ) {

        var URL = base;
        for (var i = 0, length = args.length; i < length; i++ ) {
            URL += '/' + args[i];
        }

        return URL;
    },

    getAgeFromSSN: function ( ssn ) {
        var year = ssn.substring(0, 4);
        var month = ssn.substring(4, 6);
        var day = ssn.substring(6, 8);
        var birthday = new Date( year, month, day );

        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch

        return Math.abs(ageDate.getFullYear() - 1970);

    }
};